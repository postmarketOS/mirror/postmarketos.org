# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: AGPL-3.0-or-later
import html
import os
import re
import yaml

import config.download
import config.mirrors
import config.usernames

def grid(html_str):
    """ Replace the following markers with appropriate <div class="..."> and
        </div> tags. See README.md and static/code/blog-post.css for more
        information.
        - "[#grid side#]"
        - "[#grid text#]"
        - "[#grid bottom#]"
        - "[#grid end#]"
        :param html_str: blog post code (already converted from markdown to HTML)
        :returns: html_str with all markers replaced """
    sections = ["side", "text", "bottom"]
    ret = ""
    in_grid = False

    for word in html_str.split("[#grid "):
        # Continue or start grid
        if in_grid:
            # Avoid "<p></div>"
            if ret[-3:] == "<p>":
                ret = ret[:-3]
            ret += "</div>"

        # New grid section
        tag_found = ''
        for section in sections:
            tag = section + "#]"
            if word.startswith(tag):
                tag_found = tag
                if not in_grid:
                    ret += '<div class="grid">'
                    in_grid = True
                ret += '<div class="grid-' + section + '">'
                break

        # End grid
        tag = "end#]"
        if word.startswith(tag):
            if not in_grid:
                raise ValueError("[#grid end#] found before it was opened!")
            tag_found = tag
            ret += "</div>"
            in_grid = False

        # Remove tag from word
        word = word[len(tag_found):]

        # Avoid "<div class=...></p>"
        if word[:4] == "</p>":
            word = word[4:]

        ret += word

    # Check for grids without end tag
    if in_grid:
        raise ValueError("Missing [#grid end#]!")
    return ret


def download_table(html_str):
    marker = "[#download table#]"
    if marker not in html_str:
        return html_str

    imgs_url = config.download.imgs_url
    latest_release = config.download.latest_release

    vendors = set()
    for category in config.download.table.values():
        for device in category.values():
            vendors.add(device.get("vendor"))

    filter_html = """
    <div class="table-filters">
        <select id="vendor-filter">
            <option value="">All vendors</option>
    """

    for vendor in sorted(vendors):
        filter_html += f'<option value="{vendor}">{vendor}</option>'

    filter_html += """
        </select>
        <input type="text" id="search-filter" placeholder="Search...">
    </div>
    """

    new = "<table id='devices-table' class='table-specs'>\n"
    new += "<thead><tr><th id='th-community'>Community</th><th>Stable</th><th>Edge</th></tr></thead>\n"

    for category, category_cfg in config.download.table.items():
        if category != "Community":
            new += f"<tr><td colspan='3'><b>{category}</b></td></tr>\n"

        for device, device_cfg in category_cfg.items():
            name = device_cfg["name"]
            vendor = device_cfg.get("vendor")
            link = f"{imgs_url}/{latest_release}/{device}/"
            link_edge = f"{imgs_url}/edge/{device}/"

            data_vendor = vendor or ""
            data_name = name or ""
            new += f"<tr data-vendor='{data_vendor}' data-name='{data_name}'>"
            new += f"<td class='device-name'>{name}</td>\n"
            if "not_in_stable" in device_cfg:
                new += "<td></td>\n"
            else:
                new += f"<td><a href='{link}' class='download-link' " \
                        "style='text-decoration-color: #009900bf;'>" \
                        f"{latest_release}</a></td>\n"
            new +=  "<td style='white-space: nowrap;'> "\
                   f"<a href='{link_edge}' class='download-link' " \
                    "style='text-decoration-color: orange;'>edge " \
                    "</a> 🚧</td></tr>\n"

    new += "</table>\n"
    new += """

    <script>
    document.addEventListener('DOMContentLoaded', function() {
        const vendorFilter = document.getElementById('vendor-filter');
        const searchFilter = document.getElementById('search-filter');
        const table = document.getElementById('devices-table');
        const rows = table.querySelectorAll('tbody tr');

        function filterTable() {
            const vendorValue = vendorFilter.value.toLowerCase();
            const searchValue = searchFilter.value.toLowerCase();

            main = document.getElementById('th-community');
            if (vendorValue || searchValue) {
                main.innerHTML = "Search result";
            } else {
                main.innerHTML = "Community";
            }

            rows.forEach(row => {
                const vendor = (row.getAttribute('data-vendor') || '').toLowerCase();
                const name = (row.getAttribute('data-name') || '').toLowerCase();

                const vendorMatch = vendorValue === '' || vendor === vendorValue;
                const searchMatch = searchValue === '' || name.includes(searchValue);

                row.style.display = vendorMatch && searchMatch ? '' : 'none';
            });
        }

        vendorFilter.addEventListener('change', filterTable);
        searchFilter.addEventListener('input', filterTable);
    });
    </script>
    """

    return html_str.replace(marker, filter_html + new)


def mirrors_list(html_str):
    marker = "[#mirrors list#]"
    if marker not in html_str:
        return html_str

    new = ""
    for name, mirror_cfg in config.mirrors.mirrors.items():
        urls_html = ""
        for url in mirror_cfg["urls"]:
            protocol_esc = html.escape(url.split(":", 1)[0])
            url_esc = html.escape(url)
            urls_html += f"<a href='{url_esc}'>{protocol_esc}</a> "

        location = html.escape(mirror_cfg.get("location", "Unknown Location"))

        new += f"<b>{html.escape(name)}</b><br>\n"
        new += f"{location}\n"
        if "bandwidth" in mirror_cfg:
            new += f", {html.escape(mirror_cfg['bandwidth'])}"
        new += f"<br>\n"
        new += f"{urls_html}\n"
        new += "<br><br>\n"

    return html_str.replace(marker, new)


def merge_request_templates(html_str):
    """Replace merge request templates in the form of {{MR|1234|pmaports}}."""
    while True:
        split_start = html_str.split("{{MR|", 1)
        if len(split_start) == 1:
            return html_str

        split_end = split_start[1].split("}}", 1)
        if len(split_end) == 1:
            raise RuntimeError("merge_request_templates: missing end }}}} for template starting with:"
                               f" {{{{MR|{split_start[1][:20]}")
        if " " in split_end[0]:
            raise RuntimeError("merge_request_templates: space before end }}}} in template starting with:"
                               f" {{{{MR|{split_start[1][:20]}")

        inner = split_end[0]
        components = inner.split("|")
        if len(components) != 2:
            raise RuntimeError(f"merge_request_templates: template not used correctly: {{{{MR|{inner}}}}}")

        mr_id, mr_project = components

        link = f"https://gitlab.postmarketos.org/postmarketOS/{mr_project}/-/merge_requests/{mr_id}"
        display = f"<a href={html.escape(link)} class='mr'>!{mr_id}</a>"
        html_str = split_start[0] + display + split_end[1]

    return html_str


def issue_templates(html_str):
    """Replace issue templates in the form of {{issue|1234|pmaports}}."""
    while True:
        split_start = html_str.split("{{issue|", 1)
        if len(split_start) == 1:
            return html_str

        split_end = split_start[1].split("}}", 1)
        if len(split_end) == 1:
            raise RuntimeError("issue_templates: missing end }}}} for template starting with:"
                               f" {{{{issue|{split_start[1][:20]}")
        if " " in split_end[0]:
            raise RuntimeError("issue_templates: space before end }}}} in template starting with:"
                               f" {{{{issue|{split_start[1][:20]}")

        inner = split_end[0]
        components = inner.split("|")
        if len(components) != 2:
            raise RuntimeError(f"issue_templates: template not used correctly: {{{{issue|{inner}}}}}")

        issue_id, issue_project = components

        link = f"https://gitlab.postmarketos.org/postmarketOS/{issue_project}/-/issues/{issue_id}"
        display = f"<a href={html.escape(link)}>#{issue_id}</a>"
        html_str = split_start[0] + display + split_end[1]

    return html_str


def new_templates(html_str):
    return html_str.replace("{{new}}", "<span class='new'>new</span>")


def username_templates(html_str):
    """Replace name templates in the form of {{@username}} with the first name
    (or a nickname, if the user prefers - feel free to send a patch!), and link
    it the first time it appears. This is the naming style we have been using
    since ~2024 manually, but it is automated now to save a lot of time and to
    make it more consistent."""

    found = {}
    cfg = config.usernames.names_and_links

    while True:
        split_start = html_str.split("{{@", 1)
        if len(split_start) == 1:
            return html_str

        split_end = split_start[1].split("}}", 1)
        if len(split_end) == 1:
            raise RuntimeError("username_templates: missing end }}}} for template starting with:"
                               f" {{{{@{split_start[1][:20]}")
        if " " in split_end[0]:
            raise RuntimeError("username_templates: space before end }}}} in template starting with:"
                               f" {{{{@{split_start[1][:20]}")

        username = split_end[0]
        if username not in cfg:
            raise RuntimeError(f"username_templates: missing @{username} in config/usernames.py"
                               " (if not a typo, use 'contrib/username_config_tool.py add"
                               f" {username}')")

        display = html.escape(cfg[username][0], False)
        if username not in found:
            if cfg[username][1]:
                link = html.escape(cfg[username][1])
                display = f"<a href={link}>{display}</a>"
            found[username] = True

        html_str = split_start[0] + display + split_end[1]

    return html_str


def replace(html_str):
    """ Various replacements for blog posts, to make them responsive etc.
        :param html_str: blog post code (already converted from markdown to HTML)
        :returns: html_str with replacements made """
    ret = html_str
    ret = grid(ret)
    ret = download_table(ret)
    ret = mirrors_list(ret)
    ret = username_templates(ret)
    ret = merge_request_templates(ret)
    ret = issue_templates(ret)
    ret = new_templates(ret)

    ret = ret.replace("[#latest release#]",
                      config.download.latest_release)
    return ret


def parse_yaml_md(path):
    """ Load one of the blog post / edge post / page .md files.
        :returns: data, content
                  * data: the parsed yaml frontmatter
                  * content: markdown code below "---"
    """
    with open(os.path.join(path), encoding="utf-8") as handle:
        raw = handle.read()
    frontmatter, content = config.regex_split_frontmatter.split(raw, 2)

    data = yaml.load(frontmatter, Loader=yaml.Loader)
    return data, content
