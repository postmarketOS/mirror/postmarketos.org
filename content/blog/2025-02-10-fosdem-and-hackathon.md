title: "FOSDEM 2025 + Hackathon"
date: 2025-02-10
preview: "2025-02/fosdem.jpg"
---
[![A collage of photos from FOSDEM + the hackathon](/static/img/2025-02/fosdem.jpg){: class="wfull" }](/static/img/2025-02/fosdem.jpg)

When so many people involved with postmarketOS and Linux Mobile come together in
one place, magic happens. Without even trying to solve problems, we end up
finding important solutions to long-term issues as we discuss running mainline
Linux on all sorts of devices while walking around in ULB or elsewhere in
Brussels, going by public transport or while eating in restaurants. Thanks to
everybody who came by, it was amazing!

## FOSDEM 2025

* We had a separate *postmarketOS* stand with our Linux on Mobile friends
  (Mobian, Sailfish OS, AsteroidOS, LINMOB.net, MNT) on one side and our
  friends from CalyxOS on the other. Usually we team up with the rest of the
  Linux on Mobile crowd, but given that in 2024 we *almost* ran into serious
  space issues, we figured it would be safer to request our own stand for 2025.
  And it was a blast, thanks to everybody who came by and asked us questions
  and tried out our (sometimes quite crazy) setups. It was especially nice
  seeing true tiling WM lovers experiencing Sxmo for the first time and feeling
  right at home!

* {{@f-izzo}} had printed out 150 copies of
  [the leaflet](https://gitlab.postmarketos.org/postmarketOS/artwork/-/raw/master/leaflet/leaflet_en.pdf)
  (which were all taken by visitors &mdash; except for the last two which
  people apparently didn't want to take since these were the last ones). He
  also printed
  [the entire devices wiki page](https://wiki.postmarketos.org/wiki/Devices) in
  DIN A3 format, which proved to be very useful: people ask us if we support a
  specific device all the time, so every few minutes we could whip out the
  entire print out and look for that specific device together with the visitor
  to see if it is in there or not. Regardless, we always got the same reaction:
  sooo many devices!

* On Saturday afternoon the
  [FOSS on Mobile Devices](https://fosdem.org/2025/schedule/track/mobile/)
  devroom took place, with many postmarketOS related talks such as
  [Kernel support for Mobile Linux: The missing 20%](https://fosdem.org/2025/schedule/event/fosdem-2025-4836-kernel-support-for-mobile-linux-the-missing-20-/) by
  {{@lucaweiss}},
  [Sxmo: A mobile UI for hackers](https://fosdem.org/2025/schedule/event/fosdem-2025-5926-sxmo-a-mobile-ui-for-hackers/) by {{@proycon}},
  [postmarketOS: what is it and what's new?](https://fosdem.org/2025/schedule/event/fosdem-2025-4654-postmarketos-what-is-it-and-what-s-new-/) by {{@ollieparanoid}} and many
  more! For most of them,
  the video recordings are already up by now. {{@anjandev}} also gave a talk
  called [Introduction to pmbootstrap](https://fosdem.org/2025/schedule/event/fosdem-2025-6187-introduction-to-pmbootstrap/)
  in the *Embedded, Mobile and Automotive* devroom. See also the list of related
  talks in other tracks at [LINMOB.net](https://linmob.net/fosdem2025/).

* On Sunday we recorded a new episode of the
  [postmarketOS podcast](https://cast.postmarketos.org/episode/45-FOSDEM-2025-special/).
  This episode is being released together with the blog post here and has a
  short collection of stories related to what people experienced this FOSDEM.

## Hackathon

As our team becomes bigger, so does our post-FOSDEM hackathon! This year we
rented an apartment for 12 people where mostly a mixture of Core & Trusted
Contributors got together. We had several rooms with different discussions
happening simultaneously for most of the week, which was a great success! It is
going to become harder to scale this up if the team keeps growing.

As last year, we also set a wall full of post-its to organize things to hack
and discuss together. You can see the photo of completed tasks as part of the
header image, and read more on what these were about below.

### Reliability

* Through many discussions we have concluded that our main goal for 2025 is:
  **Improve the reliability of postmarketOS!**

* One major part of getting there is automated hardware testing. Once we have
  that in place, we can continuously verify whether features are still working
  on devices we test. Regressions will be caught early, ideally before we even
  merge changes, and we should be able to fix them much more quickly. This
  should help us get rid of the days on which you upgrade your edge install and
  have no way of knowing whether audio will still work after the upgrade, for
  example.

* We followed up on the
  [hardware CI](https://gitlab.postmarketos.org/groups/postmarketOS/-/milestones/26)
  project that was started in
  [2024-09](/blog/2024/09/29/pmOS-update-2024-09/#organizational). There we
  originally intended to pay a developer to design a PCB for connecting the
  phones in our future automated hardware testing setup. Unfortunately they
  didn't have time and so we didn't make much progress on that topic up until
  the hackathon. However having enough skilled people in one place worked magic
  and we figured that our TCs {{@f-izzo}} and {{@anjandev}} could take this
  over with support from {{@caleb}}. We are happy to report that they have
  already started and this is now back on track!

* The result will of course be under an open license, and we want to make it so
  that others in the community can get the PCB too and use it to add hardware
  CI for their favorite device as well.

* In addition, we have started preparing for the software work that will be
  necessary to actually *do* testing on real hardware. This part of the project
  involves all the infrastructure that allows the GitLab CI to communicate with
  the devices, send it the jobs to execute, and receive the artifacts. For this
  part of the project, we intend to use
  [CI-tron](https://gitlab.freedesktop.org/gfx-ci/ci-tron),
  and will be working with both its maintainer as well as an experienced member
  of the community to implement the missing pieces and integrate it with our
  GitLab. This will be funded entirely through our
  [OpenCollective](https://opencollective.com/postmarketOS)!
  Once the agreement is in place, we will provide a detailed update on the
  architecture and our plan for implementing it. Stay tuned!

* As a follow-up from this project, and with the idea to improve reliability,
  {{@anjandev}}, {{@f-izzo}}, together with {{@DylanVanAssche}} planned on
  how to possibly test calls on CI. They made some progress planning, see
  {{issue|96|postmarketos}}!

### Camera

* While they were not at the hackathon, {{@DrGit}} surprisingly sent patches
  that made one of the rear cameras and the front camera on the OnePlus 6 and
  OnePlus 6T work for the first time! {{@caleb}} prepared
  {{MR|6148|pmaports}} based on that, and made a
  [call for testing](https://social.treehouse.systems/@cas/113948470705493029)
  and soon lots of people posted initial photos and even videos of the camera
  in action in the same Mastodon thread. This feature has been requested for
  such a long time, it is amazing to see this moving forward!

* {{@caleb}} and {{@dh}} looked into getting the not-yet enabled C-PHY mode
  working.

* {{@Minecrell}} worked on the BQ Aquaris X5 camera on top of the work by
  {{@a_a}} on the BQ Aquaris M5.

### Device ports

* {{@lucaweiss}} proposed to clearly differentiate devices in the testing
  category between ports based on mainline and downstream kernels. (All higher
  device categories, community and main, already require mainline kernels).
  This was discussed and turned into actionable items, see
  [milestone 30](https://gitlab.postmarketos.org/groups/postmarketOS/-/milestones/30)
  for details. Contributions welcome!

### systemd

* {{@craftyguy}}, together with external support from {{@rmader}} finished
  enabling sensor support on systemd ({{MR|6147|pmaports}}).

* {{@craftyguy}}, {{@pabloyoyoista}}, {{@jane400}}, {{@ollieparanoid}},
  {{@caleb}} discussed how to finish upstreaming the abuild systemd split
  function ({{issue|2804|pmaports}}). We discussed different alternatives and
  decided that it is probably best to follow up with abuild maintainers
  regarding what solution they would prefer the most.

* {{@ollieparanoid}} and {{@jane400}} discussed how to make Phosh work again
  with postmarketOS edge and systemd since it was currently broken there. We
  decided to replace the current, not working `phosh.service` implementation
  with tinydm (the same way it currently works with OpenRC). {{@fossdd}}
  had then implemented this change in {{MR|6150|pmaports}}. The plan is to
  later on use greetd with phrog instead of tinydm, and to use that for both
  systemd and OpenRC. {{@joelselvaraj}} and {{@samcday}} are working on this in
  {{MR|6106|pmaports}}.

### Organizational

* {{@pabloyoyoista}}, {{@ollieparanoid}}, {{@Newbyte}}, {{@lucaweiss}},
  {{@PureTryOut}} managed to finish up the Request for Change process we now
  call postmarketOS Change Request
  ([PMCR](https://gitlab.postmarketos.org/postmarketOS/pmcr/-/blob/main/0001-pmcr-process.md)).
  With that we finally have a place to discuss bigger changes to the project
  with a defined structure. This led to the second
  [PMCR](https://gitlab.postmarketos.org/postmarketOS/pmcr/-/merge_requests/2)
  being sent within hours! We also had a fun time trying to figure out an
  original name of the process (so it is immediately clear that people are not
  just referring to *any* RFC, but a postmarketOS specific RFC). We considered
  PMEP at one point (postmarketOS Enhancement Proposal) but since that is
  impossible to pronounce if you try to say it like PEP from Python, we went
  with PMCR :)

* During our first hackathon after FOSDEM 2024 we set some projects as
  priorities for the postmarketOS team that year. Now one year later we have
  reviewed our progress and improved on how we approach these kinds of mid-term
  milestones. We will soon provide an update on this blog with further details
  (either as separate blog post or as part of the upcoming monthly blog post)!

* In addition to the mid-term project priorities, we also started discussing a
  possible 5 year strategic plan. postmarketOS is in a really amazing position
  with a healthy and productive community. We want to ensure this continues to
  be the case as the project grows, but we also want to continue pushing the
  envelope and see where it leads. We have some ideas, and did some initial
  discussions at the hackathon. However properly answering this will likely
  require further discussions and planning with the rest of the team and the
  community, so we can't write much about this yet.

### Infrastructure

* {{@pabloyoyoista}} added support in
  [aports-qa-bot for pinging maintainers](https://gitlab.alpinelinux.org/alpine/infra/aports-qa-bot/-/merge_requests/100)
  when somebody proposes changes to packages they
  maintain. This should make it easier to maintain packages. Previously
  maintainers wouldn't always be notified when changes to their packages were
  made &mdash; even though they were in CODEOWNERS, but notifying through that
  is a GitLab premium feature that we don't have on our self-hosted instance
  anymore. The bot is already live (deployed by {{@lucaweiss}}) and commenting
  away!

* {{@caleb}}, {{@ollieparanoid}} and {{@craftyguy}} tweaked GitLab CI configs
  and eventually created a custom GitLab runner to fix that QEMU-related jobs
  didn't run reliably since we had switched to gitlab.postmarketos.org.

* {{@caleb}}, {{@pabloyoyoista}} and {{@ollieparanoid}} worked
  towards integrating [marge-bot](https://gitlab.com/marge-org/marge-bot/)
  into our development process. This will allow us to automate manual steps
  that we currently do before merging (rebase, put MR ID into commit
  messages, wait for CI), and therefore accelerate our development process.
  It also takes care of merging ready MRs in order, avoiding conflicts we
  can currently run into when two developers try to merge MRs at the same
  time. We talked to {{@dh}} from upstream who patiently answered all of our
  questions and found a way forward for all features that
  we need before we can use marge-bot
  ([userfriendly messages (implemented!)](https://gitlab.com/marge-org/marge-bot/-/merge_requests/518),
  and commit signing (TBD)).

* {{@PureTryOut}} and {{@lucaweiss}} did an upgrade of all our matrix rooms, in
  order to fix state resets that a couple of people have been getting as well
  as making permissions consistent across all rooms.

* [BPO](https://gitlab.postmarketos.org/postmarketOS/build.postmarketos.org/),
  our build orchestrator got a security audit from
  [Radically Open Security](https://www.radicallyopensecurity.com/) as part of
  the [postmarketOS daemons](https://nlnet.nl/project/postmarketOS-daemons/)
  grant from NGI Zero Core / NLnet. The audit was successfully kicked off during
  the hackathon (from our end {{@ollieparanoid}} explained how to run tests
  locally, answered follow-up questions etc). While we still need to get the
  final report, it looks like there were no remarkable security concerns.

### Other

* {{@jane400}} spent some time looking into a compatible solution for GNOME
  Clocks to set alarms that will work even on suspend.

* We discussed at length how to do taxes for invoices to OpenCollective.
  {{@jane400}} has done some initial systemd upstreaming work that we still
  need to figure out the VAT situation for. We have fortunately gotten some
  support from KDE e.V. and we will be contacting some tax lawyer in the short
  term. This work is not very enjoyable, but very important.

* {{@ollieparanoid}} and {{@craftyguy}} finished a MR that has been open for a
  long time, which moves the [contribute](/contribute) page from the wiki to
  our website and modernizes it. Thanks {{@RannyBergamotte}} for the initial
  work on it, and for adding all these cute postmarketOS logo variations!
  Together with {{@pabloyoyoista}} we also made progress moving some important
  governance information from different scattered places to the website
  ({{MR|335|postmarketos.org}}).

* We discussed how to better organize ourselves and go to conferences. We should
  start keeping an inventory for merchandising and marketing materials, but also
  plan and better support people from the community going to them. We want
  postmarketOS to have visibility in many more places, and not just very
  technical conferences. But we need to  make sure those spreading the word
  have the support they deserve! In the following weeks we will be creating a
  repository to better organize and plan this effort.

* For hackathon movie night, we chose *For The Plasma (2014)* this time (see
  [our reviews](https://fosstodon.org/@postmarketOS/113947694077980141)) to
  balance out the GNOME themed movie *The GNOME Mobile (1967)* from last year.
  Let us know if you have similar movie suggestions for next year, maybe
  something that relates to SXMO or tiling window managers! For the last night,
  {{@PureTryOut}} had a Wii emulator set up where some of us played a few
  rounds of Mario Party.

* Cooking and shopping for 12 people is a challenge. Thanks to {{@lucaweiss}}
  and {{@f-izzo}} who cooked some delicious meals that kept all hackers going!

* Although this is not a short blog post, this doesn't cover everything that
  was going on. We didn't make sticky notes for each topic we worked on and we
  had many, many important conversations that would be hard to fit in here.


Having this hackathon was only possible thanks to *your* donations! Just like
last year, it was incredibly productive to have everybody in the same place,
for planning, discussing goals and for just getting things done quickly without
delay.

If you appreciate the work we're doing with postmarketOS and want to support us,
[consider contributing financially via OpenCollective](https://opencollective.com/postmarketos).
