title: "postmarketOS Release: v21.06 Service Pack 3"
date: 2021-10-10
preview: "2021-10/oled-theme.jpeg"
---

[![](/static/img/2021-10/oled-theme_thumb.jpg){: class="wfull border"}](/static/img/2021-10/oled-theme.jpeg)

New month, new service pack! As always, new features and fixes have been
backported from postmarketOS edge to the stable version after tough testing by
our tough community members on edge.

## Overview

* [Sxmo](https://sxmo.org) 1.5.2: as usually we've coordinated with the Sxmo
  team on what should be in the service pack, and thanks to them, got Sxmo
  1.5.2 in. This is the tried and tested
  [1.5.1](https://lists.sr.ht/~mil/sxmo-announce/%3C2ORBA75Z8VN74.3PGSSQF7MP773%40stacyharper.net%3E)
  version from edge, with just minor extra fixes on top.
* [Tweaks 0.8.1](https://gitlab.com/postmarketOS/postmarketos-tweaks/-/tags/0.8.1)
  has a new sound panel, better notch support, some bugfixes for the background
  daemon and fixed touch navigation on the mobile layout.
* [linux-postmarketos-qcom-sdm845 5.14](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/2416)
  improves the OnePlus 6 and 6T ports: among other improvements it ships
  initial modem support and more GPU performance.
* [ModemManager 1.18.2](https://gitlab.freedesktop.org/mobile-broadband/ModemManager/-/blob/mm-1-18/NEWS)
  was required for modem support in the SDM845 kernel. As nice benefit it
  should improve call and SMS handling.
* [NetworkManager 1.32.10](https://gitlab.freedesktop.org/NetworkManager/NetworkManager/-/blob/1.32.2/NEWS)
  was backported to fix hotspot functionality while having the firewall
  enabled.
* [postmarketos-theme](https://gitlab.com/postmarketOS/postmarketos-theme) can
  now be installed to get GTK 3 and GTK 4 postmarketOS themes (see photo).

## Details

### ModemManager

This release backports the calling stack from edge. The new release of ModemManager brings a lot of improvements
for the devices supported in community and will help a lot with the stability of the modem when the PinePhone
awakes from deep sleep. This release also adds support for the modem in Qualcomm Snapdragon 845 devices. The full
calling functionality on those devices is not working yet but it means that at least mobile data and SMS is usable.

### postmarketos-theme
This release also includes the new postmarketOS theme as an optional package. This currently contains a GTK 3 and GTK 4
theme which is based on Adwaita but replaces the standard blue highlights in the theme with postmarketOS green. There's
also the oled and paper variant of the theme that are completely black and completely white. This will mainly look great
on oled displays, as the name suggests. It's possible to try this on your own device now by installing the
`postmarketos-theme` package and selecting one of the four themes in postmarketOS tweaks.

### Tweaks
postmarketOS tweaks also has a new release in stable now that contains a series of fixes from community members.
One of the most visible changes is the new sound panel that allows changing the GTK sound theme but also allows making
a custom theme by manually specifying the audio files for every event. Another change is the notch tweaks for Phosh
that build on top of the CSS improvements of the last update. This allows moving Phosh UI elements over to make them
not appear under the notch of modern smartphones.

## How to get it
Find the most recent images at our [download page](/download/). Existing users
of the v21.06 release will receive this service pack automatically on their
next system update.

Thanks to everybody who made this possible, especially our amazing community
members and upstream projects.

## Known issues
Samsung Galaxy S4 Mini Value Edition (samsung-serranove) users have experienced
a regression with the new Modemmanager version. Details and workaround in
[pma#1274](https://gitlab.com/postmarketOS/pmaports/-/issues/1274).

## Comments

* [Mastodon](https://fosstodon.org/@postmarketOS/107078180299976794)
* [Lemmy](https://lemmy.ml/post/84759)
<small>
* [HN](https://news.ycombinator.com/item?id=28819765)
</small>
