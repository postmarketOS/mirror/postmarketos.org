title: "FOSDEM 2024 + Hackathon"
date: 2024-02-14
preview: "2024-02/fosdem-cover.jpeg"
---

[![A collection of stickers and phones at the Linux on Mobile FOSDEM stand](/static/img/2024-02/fosdem-cover.jpeg){: class="wfull border" }](/static/img/2024-02/fosdem-cover.jpeg)

As is quickly becoming tradition, the postmarketOS team made their way to FOSDEM
at the beginning of February again. We rarely get the chance to have the entire
core team in the same place (unfortunately we still had Dylan missing). As such
we took the chance to not only take part in this wonderful conference in
Brussels, but also spend three additional days hacking away in a nice
distraction-free apartment that we booked, powered by the donations we receive
on OpenCollective. It was extraordinary, we had so much focus, did an enormous
amount of planning and tackling of hard tasks, wrote new code,
[removed](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/4732)
legacy code, worked on infrastructure and just had a good time. There is a lot
coming up, you can expect to read more in blog posts and merge requests in the
next weeks and months. That being said, we wanted to share some of the
highlights, especially for the people supporting us. Without you, this would
have been significantly harder to organise.

### FOSDEM Saturday

[#grid side#]
[![Us at the FOSDEM Linux on Mobile stand](/static/img/2024-02/fosdem-stand.jpg){: class="w300 border" }](/static/img/2024-02/fosdem-stand.jpg)
<br><span class="w300">
By the fact that Clayton lost his voice on Saturday, you can tell it was busy.
</span>
[#grid text#]
Compared to a typical Saturday morning, some of us got up really early to
prepare our corner of the [Linux on Mobile](https://linuxonmobile.net/) stand,
together with all the other amazing projects. It involved some last minute
flashing, placing stickers on the table, configuring the phones to not blank
their screen and so on. Some of us stayed at the table for more or less the
whole day and had incredible conversations with all the people coming by.
Compared to last year where we were in the H building, being in Aw (awwww!)
seemed a bit more remote
([map](https://archive.fosdem.org/2023/assets/campusmap-6166e45e7e736823c218c45ac65e02f5f7237111253db302da17bbaa0f4b5683.png))
and one might think that this would lead to less people coming by. But far from
it!
[#grid end#]

FOSDEM is known for many things taking place in parallel and so did, in
addition to the countless stands, an incredible amount of devrooms have talks
going on at the same time. One of these devrooms was
*FOSS on Mobile Devices*. Again a collaboration with our friends from projects
such as Mobian, Sailfish OS, Ubuntu Touch, PureOS and NemoMobile. Folks
related to postmarketOS gave a few talks and helped running the devroom. At the
same time, quite a few people streamed the talks and had some good questions in
the Matrix chat that were asked to the presenters in the short Q&A sessions
after each talk. The devroom was a total blast, we were happy to have so many
amazing presentations there! All talks were recorded and
[have been uploaded](https://fosdem.org/2024/schedule/track/foss-on-mobile-devices/)
if you have some hours to kill.

After everyone left ULB in the evening, we spontaneously decided to go to the
GNOME Beers event. Even our two renown KDE devs Bart and Luca joined in and
made it out alive!

### FOSDEM Sunday

As the devroom was "only" taking place the whole Saturday, the people who spent
all day in it could get the full stand experience. Thanks to help from the Sxmo
IRC channel, we figured out how to make the Sxmo demo phone not blank its
screen as well (it is actually a standard feature that can be accessed through
the menu). We made a post-it with instructions for our favorite tiling window
manager based phone UI and people were happy to give it a try and to learn the
combinations (if you really try and have a guide in front of you, you can get
the hang of it in a couple of minutes). We had many more great conversations,
and handed out a lot of stickers. Thanks to everybody who came by, it was
amazing!

Again, in parallel, talks were visited, and at the end of the day we were
surprised to be not kicked out of the cafeteria after FOSDEM was basically
over. Big cheers to Raffaele, Peter and Sebastian for sticking around and
joining us in recording
[postmarketOS podcast #38](https://cast.postmarketos.org).

### Hackathon

One of the first things we did on Monday morning, was taking pictures off from
a wall and use it for nice visual planning with post-its. As mentioned earlier,
we will make separate blog posts for the bigger things in the near future, but
here are some of the topics that we discussed and worked on with intense focus:

* Did a big prioritization of projects within postmarketOS, such as VoLTE,
  cameras, HW testing-setup, PipeWire and power management. To drive them
  forward effectively, we matched them with possible grants and funding
  opportunities and contected people from the community who could possibly work
  on them. (If you are interested and really have knowledge in one of these
  areas, reach out to us!)

* Moving forward with migrating away from gitlab.com
  ([why?](https://gitlab.com/postmarketOS/postmarketos/-/issues/49#note_1733384851)).
  We have two promising new places where we could go, and did a video call with
  one of them.

* Discussed potentially shipping Flatpak packages from Flathub by default. From
  discussions with both KDE and GNOME people, it is clear that this is their
  preferred choice of how users should install their apps. By now probably
  everybody knows the upsides of sandboxing and having a clear path for
  shipping updated apps with a stable base system (i.e. new apps on
  postmarketOS stable, without backporting them through a service pack). The
  downsides are that not all devices have enough space or resources for them,
  and that Flathub only supports x86_64 and aarch64 (no 32-bit architectures or RISC-V). We still need to figure
  out the details, but the idea is to ship it by default for the UIs and device
  combinations where it makes sense in terms of being desired by upstream (the
  folks providing the UI) and in terms of having a great experience on the
  device. And that if you build your own image, you can just choose it as you
  prefer.   See {{MR|4820|pmaports}} and {{MR|2254|pmbootstrap}} for more
  information.

* Worked on improving the status and handling of non-free firmware. The thing
  is, it would be very nice if you could use your phone without non-free
  firmware. But in practice, the mechanism we have right now is hard to
  maintain, not well tested, and consequently you would sometimes even get an
  image with non-free firmware installed even though you selected that you
  don't want to have it. Furthermore, in modern phones you sometimes cannot
  even charge your phone without proprietary firmware. So in reality the
  feature of opting out of non-free firmware doesn't really work anymore and we
  will probably just remove it. {{MR|2255|pmbootstrap}} works towards that.

* Had a very fruitful discussion about device maintainership and
  categorization. We discussed on how to improve the status of main and
  community devices, and possible ideas to help maintainers in their tasks.
  We have had a very positive experience with the creation of the Trusted
  Contributors procedure, and we hope to be able to replicate the success when
  it comes to device maintenance. You can expect a blog post about this soon™.
  If you are curious, our notes are in {{issue|59|postmarketos}}.

* Applied to GSoC with some smaller, yet very useful
  [project ideas](https://postmarketos.org/gsoc/). We have not done this
  before, but trying it out at least once seems useful.

* Reviewed pmbootstrap patches, made new performance and other improvements to
  it and released [2.2.0](https://gitlab.com/postmarketOS/pmbootstrap/-/tags/2.2.0).
  While at it, we made all PyPI releases deprecated - since pmbootstrap does
  not have dependencies on other Python packages it does not offer a benefit
  for us while at the same time adding a few more steps to the release process.
  We have been recommending to install from either
  [distros' package managers](https://wiki.postmarketos.org/wiki/Pmbootstrap#From_package_manager)
  or [git](https://wiki.postmarketos.org/wiki/Pmbootstrap#From_git) for quite
  some time now.

* Improvements to the infrastructure were made, and we continued to move
  services over from the old server to the new one. With the work in January
  we already managed to get the infrastructure stable again (back when
  everything was running on the old server, it really wasn't in a good state).
  Now the goal is to move the rest over as well, so we don't need to pay for
  the old and less reliable server anymore in addition to the nice new one.

* Did a lot of smaller tasks, such as archiving Megapixels in the pmOS
  namespace (now that Martijn made a
  [new release](https://gitlab.com/megapixels-org/Megapixels/-/tags/1.8.0) in
  his megapixels-org namespace, which also holds WIP code for
  [Megapixels 2.0](https://blog.brixit.nl/megapixels-2-0/), yay!), and moving
  forward with integrating the new wallpapers ({{MR|4716|pmaports}}).

* Watched [The Gnome Mobile](https://fosstodon.org/@postmarketOS/111886441429318021),
  don't miss the reviews.


On reflection, this has possibly been one of the most productive (and fun) weeks
for the project in a very long while. And your donations have been a clear
enabler for this to happen. So if you appreciate the work we're doing on
postmarketOS, and want to support us, consider
[joining our OpenCollective](https://opencollective.com/postmarketos).
