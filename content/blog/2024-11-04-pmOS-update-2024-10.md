title: "postmarketOS in 2024-10: Accepted Grants, Timelines and Tokyo"
date: 2024-11-04
preview: "2024-10/monthly.jpg"
---

[![Rob and Masanori sitting at a table with postmarketOS logo, flyers, devices at Tokyo Open Source Conference](/static/img/2024-10/monthly.jpg){: class="wfull" }](/static/img/2024-10/monthly.jpg)

For the first time postmarketOS was represented at an event in Japan thanks to
{{@longnoserob}} and {{@omasanori}} who went to the
[Open Source Conference 2024 Tokyo/Fall](https://event.ospn.jp/osc2024-fall/)!
As shown in the photo above, they had a cool table with
[asus-grouper](https://wiki.postmarketos.org/wiki/Google_Nexus_7_2012_(asus-grouper)),
[asus-tf201](https://wiki.postmarketos.org/wiki/ASUS_Transformer_Prime_(asus-tf201)),
[xiaomi-beryllium](https://wiki.postmarketos.org/wiki/Xiaomi_POCO_F1_(xiaomi-beryllium)),
[pine64-pinephone](https://wiki.postmarketos.org/wiki/PINE64_PinePhone_(pine64-pinephone))
as well as stickers and flyers in Japanese (which ran out by the end!) and
English. Several visitors mentioned that they were astonished how fast the
tf201 and grouper appeared in the shell - and in general they were interested
in seeing devices running with (near) mainline kernels. Thanks to everybody who
came by! If you are in the area and missed the event, a Spring edition is
coming up in February 2025.


## Organizational

### NLnet Grants Accepted

{{@pabloyoyoista}} and {{@ollieparanoid}} helped members of the Linux Mobile
community to apply for grants from
[NLnet's](https://nlnet.nl/) [NGI Zero Core](https://nlnet.nl/core/)
fund, and we are excited to announce that they have been accepted!

* {{@biktorgj}} and {{@lynxis}} will be working on
  [OpenIMSd](https://nlnet.nl/project/VoLTE-Qualcom/), which aims to bring
  VoLTE (4G voice calls) to Qualcomm based phones (like the PinePhone) running
  Free Software Mobile Operating Systems including postmarketOS, Mobian and
  others. They will create a daemon which runs in parallel to ModemManager,
  which configures the baseband via QMI and brings up all the required
  services to be able to place VoLTE calls.

* {{@dannycolin}} will be able to improve
  [mobile-config-firefox](https://gitlab.postmarketos.org/postmarketOS/mobile-config-firefox/)
  and replace hacks we currently carry there to make the desktop version of
  Firefox work on mobile, with proper solutions in upstream Firefox. See
  [Enhancing Firefox for Linux on Mobile](https://nlnet.nl/project/Firefox-linuxmobile/)
  for details.
  [Milestones](https://gitlab.postmarketos.org/postmarketOS/mobile-config-firefox/-/milestones?search_title=nlnet)
  have been created and will be filled with related issues and merge requests.

### Trusted Contributors

* {{@JustSoup321}} is a new Trusted Contributor! They have been contributing to
  postmarketOS a lot over the past months, especially the
  [Lomiri](https://wiki.postmarketos.org/wiki/Lomiri) packaging, systemd
  support and design ideas for
  [optionally immutable postmarketOS](https://gitlab.postmarketos.org/groups/postmarketOS/-/milestones/25).

* {{@susurrus}} stepped down as TC, and we thank him for the great improvements
  he has made to the project in the short time as TC: a lot of help with
  organizing ourselves better, reworking the homepage navigation and
  adding [team pages](/team/) (which also resulted in the amazing redesign!),
  work on mainlining the
  [Galaxy Tab 4](https://wiki.postmarketos.org/wiki/Samsung_Galaxy_Tab_4)
  family of devices and writing blog posts.

### GitLab Migration

* We have migrated from gitlab.com to our own instance at
  gitlab.postmarketos.org, see this separate post for details:
  [Why and how we migrated from gitlab.com to gitlab.postmarketos.org](/blog/2024/10/14/gitlab-migration/)

* The migration was followed by a lot of clean up work, and for the most part
  the new instance is functional. Sometimes cross compilation CI jobs are
  failing ({{issue|3275|pmaports}}), we are working on it.

### systemd timeline

In last month's [And what's next?](/blog/2024/09/29/pmOS-update-2024-09/#and-whats-next)
section it seemed feasible that we could merge the systemd branch into edge in
October. However the GitLab migration and related clean up tasks took longer
than expected, and so we were not able to work on systemd blockers like
{{issue|140|build.postmarketos.org}} as much.

In a recent team meeting we had to decide whether to include systemd in the
upcoming v24.12 release or not (so we have enough time for testing and
follow-up bug fixing). As we talked through it, we realized that we would
likely burn ourselves out by attempting to have it in v24.12. It was not easy,
but we decided on the new plan to merge it into edge *after* v24.12
is out and to ship it in a stable release for the first time with v25.06.

## pmbootstrap

* Work on the upcoming
  [pmbootstrap v3](https://connolly.tech/posts/2024_06_15-pmbootstrap-v3/)
  has been going on for several months now. It is reliable enough that we
  can use it pretty much everywhere in our own infrastructure (edge jobs for
  CI, building packages and images, monitoring, etc.) We slimmed down the
  related
  [milestone](https://gitlab.postmarketos.org/postmarketOS/pmbootstrap/-/milestones/1)
  to the critical path of what is really needed to make the release and plan to
  have it done before v24.12.

* {{@Newbyte}} joined {{@ollieparanoid}} and {{@caleb}} in maintaining
  pmbootstrap. {{@Newbyte}} also made a lot of improvements to the code base,
  mostly to get v3 ready for release and to improve typing
  ({{MR|2411|pmbootstrap}}/[imported](https://gitlab.com/postmarketOS/pmbootstrap/-/merge_requests/2411),
   {{MR|2415|pmbootstrap}}/[imported](https://gitlab.com/postmarketOS/pmbootstrap/-/merge_requests/2415),
   {{MR|2419|pmbootstrap}}/[imported](https://gitlab.com/postmarketOS/pmbootstrap/-/merge_requests/2419),
   {{MR|2421|pmbootstrap}}/[imported](https://gitlab.com/postmarketOS/pmbootstrap/-/merge_requests/2421),
   {{MR|2428|pmbootstrap}},
   {{MR|2424|pmbootstrap}},
   {{MR|2432|pmbootstrap}},
   {{MR|2425|pmbootstrap}},
   {{MR|2431|pmbootstrap}},
   {{MR|2441|pmbootstrap}},
   {{MR|2434|pmbootstrap}},
   {{MR|2450|pmbootstrap}},
   {{MR|2442|pmbootstrap}},
   {{MR|2464|pmbootstrap}},
   {{MR|2466|pmbootstrap}}).
   Thanks {{@Newbyte}}!

* SHA1 RSA signatures are considered insecure nowadays, for that reason they
  have been disabled in Fedora 41.
  {{@PureTryOut}} adjusted
  [apk-tools](https://gitlab.com/postmarketOS/pmbootstrap/-/merge_requests/2417)
  in Alpine to also have a SHA256 RSA signature and {{@craftyguy}} made
  pmbootstrap to use this signature
  ({{MR|2417|pmbootstrap}}/[imported](https://gitlab.com/postmarketOS/pmbootstrap/-/merge_requests/2417),
   {{MR|2465|pmbootstrap}}).
  This fix has been backported to pmbootstrap
  [2.3.3](https://gitlab.postmarketos.org/postmarketOS/pmbootstrap/-/tags/2.3.3),
  so it works on Fedora 41. Thanks {{@craftyguy}}, {{@PureTryOut}}!

* In order to be able to set PipeWire/PulseAudio as default per UI (which makes
  sense in a transitional phase until all UIs can use PipeWire) and similar use
  cases, pmbootstrap can now use a custom `_pmb_default` key from APKBUILDs
  ({{MR|2301|pmbootstrap}}/[imported](https://gitlab.com/postmarketOS/pmbootstrap/-/merge_requests/2301)).
  Thanks {{@JustSoup321}}, {{@craftyguy}}!

* pmbootstrap now uses gitlab.postmarketos.org for new pmaports.git clones and
  can migrate users from the previous (archived) gitlab.com URL to the new
  URL in their already checked out pmaports. This has been backported to
  [2.3.2](https://gitlab.postmarketos.org/postmarketOS/pmbootstrap/-/tags/2.3.2).
  ({{MR|2427|pmbootstrap}},
   {{MR|2443|pmbootstrap}},
   {{MR|2445|pmbootstrap}},
   {{MR|2447|pmbootstrap}},
   {{MR|2429|pmbootstrap}},
   {{MR|2460|pmbootstrap}}).
  Thanks {{@lucaweiss}}, {{@Newbyte}}, {{@caleb}}, {{@ollieparanoid}}!

* "pmbootstrap install" now uses GPT (GUID Partition Table, not the AI thing)
  instead of MBR by default and implements the
  [Discoverable Partitions Specification](https://uapi-group.org/specifications/specs/discoverable_partitions_specification)
  ({{MR|2426|pmbootstrap}},
   {{MR|5707|pmaports}}).
  Thanks {{@JustSoup321}}, {{@sicelo}}!

* The CI jobs for generating pmbootstrap docs can now run in pmbootstrap's
  chroots. This is useful so users running this CI script locally don't need to
  install sphinx dependencies on the host system
  ({{MR|2413|pmbootstrap}}/[imported](https://gitlab.com/postmarketOS/pmbootstrap/-/merge_requests/2413)).
  Thanks {{@lucaweiss}}!

* Lots of additional automated testing via integration tests
  ({{MR|2444|pmbootstrap}},
   {{MR|2453|pmbootstrap}}).
  Thanks {{@craftyguy}}, {{@caleb}}!

* Fixes for making sure pmbootstrap works with Python >= 3.10 as well as
  running our CI tests against both 3.10 and 3.12
  ({{MR|2455|pmbootstrap}}, {{MR|2471|pmbootstrap}}).
  Thanks {{@caleb}}!

* pmbootstrap was still running apk through QEMU in some cases, the related
  code has been reworked so this is no longer the case
  ({{MR|2463|pmbootstrap}}).
  Thanks {{@caleb}}!

* Lots of smaller fixes and improvements
  ({{MR|2416|pmbootstrap}}/[imported](https://gitlab.com/postmarketOS/pmbootstrap/-/merge_requests/2416),
   {{MR|2423|pmbootstrap}}/[imported](https://gitlab.com/postmarketOS/pmbootstrap/-/merge_requests/2423),
   {{MR|2433|pmbootstrap}},
   {{MR|2412|pmbootstrap}}/[imported](https://gitlab.com/postmarketOS/pmbootstrap/-/merge_requests/2412),
   {{MR|2439|pmbootstrap}},
   {{MR|2438|pmbootstrap}},
   {{MR|2446|pmbootstrap}},
   {{MR|2440|pmbootstrap}},
   {{MR|2346|pmbootstrap}}/[imported](https://gitlab.com/postmarketOS/pmbootstrap/-/merge_requests/2346),
   {{MR|2449|pmbootstrap}},
   {{MR|2448|pmbootstrap}},
   {{MR|2454|pmbootstrap}},
   {{MR|2451|pmbootstrap}},
   {{MR|2456|pmbootstrap}},
   {{MR|2457|pmbootstrap}},
   {{MR|2458|pmbootstrap}},
   {{MR|2461|pmbootstrap}},
   {{MR|2459|pmbootstrap}},
   {{MR|2462|pmbootstrap}},
   {{MR|2470|pmbootstrap}}).
  Thanks {{@lucaweiss}}, {{@caleb}}, {{@craftyguy}}, {{@alexeymin}}, {{@ollieparanoid}},
  {{@jane400}}, {{@sixthkrum}}!


## pmaports + aports

* User interfaces: GNOME was upgraded to 47.1, Phosh was upgraded to 0.42.1, KDE Plasma was
  upgraded to 6.2.2. Thanks {{@fossdd}}, {{@Newbyte}}, {{@PureTryOut}},
  {{@rmader}}, {{@ptrcnull}}!

* Lots of kernel packaging patches, mostly upgrades to 6.11
  ({{MR|5399|pmaports}}/[imported](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/5399),
   {{MR|5557|pmaports}}/[imported](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/5557),
   {{MR|5640|pmaports}}/[imported](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/5640),
   {{MR|5608|pmaports}}/[imported](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/5608),
   {{MR|5624|pmaports}}/[imported](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/5624),
   {{MR|5669|pmaports}}/[imported](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/5669),
   {{MR|5611|pmaports}}/[imported](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/5611),
   {{MR|5672|pmaports}}/[imported](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/5672),
   {{MR|5486|pmaports}}/[imported](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/5486),
   {{MR|5602|pmaports}}/[imported](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/5602),
   {{MR|5686|pmaports}}/[imported](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/5686),
   {{MR|5731|pmaports}},
   {{MR|5741|pmaports}},
   {{MR|5753|pmaports}},
   {{MR|5705|pmaports}},
   {{MR|5702|pmaports}},
   {{MR|5709|pmaports}},
   {{MR|5690|pmaports}},
   {{MR|5725|pmaports}},
   {{MR|5716|pmaports}},
   {{MR|5745|pmaports}}).
  Thanks {{@craftyguy}}, {{@Newbyte}}, {{@jianhua}}, {{@barni2000}},
  {{@Arnavion}}, {{@adamthiede}}, {{@dujem}}, {{@caleb}}, {{@lucaweiss}},
  {{@joelselvaraj}}, {{@akemnade}}, {{@alexeymin}}, {{@rdacayan}},
  {{@MightyM17}}!

* New devices:
  [Google Pixel 3](https://wiki.postmarketos.org/wiki/Google_Pixel_3a_(google-sargo))
  ({{MR|5514|pmaports}}/[imported](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/5514)),
  [a generic (!!) qcom-msm8953 device](https://wiki.postmarketos.org/wiki/Generic_MSM8953_(qcom-msm8953))
  ({{MR|5692|pmaports}}),
  [Sony Xperia Z Ultra](https://wiki.postmarketos.org/wiki/Sony_Xperia_Z_Ultra_(sony-togari)) ({{MR|5739|pmaports}}).
  Thanks {{@phodina}}, {{@caleb}}, {{@barni2000}}, {{@kevinwidjaja21}}!

* A bunch of work went into making libcamera work on ex-Android devices
  ({{MR|5615|pmaports}}/[imported](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/5615),
   {{MR|5623|pmaports}}/[imported](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/5623),
   {{MR|5626|pmaports}}/[imported](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/5626),
   {{MR|5628|pmaports}}/[imported](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/5628),
   {{MR|5644|pmaports}}/[imported](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/5644),
   {{MR|5710|pmaports}}).
  One notable result is
  [Pixel 3A rear camera enablement](https://mastodon.social/@rmader/113323807229802841)!
  Thanks {{@rmader}}, {{@alistair23}}, {{@craftyguy}}!

* Initramfs improvements to be able to drop the minimal initramfs and
  making initramfs-extra opt-in
  ({{MR|5636|pmaports}}/[imported](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/5636),
   {{MR|5621|pmaports}}/[imported](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/5621),
   {{MR|5751|pmaports}}).
  Thanks {{@caleb}}, {{@lucaweiss}}, {{@craftyguy}}!

* [Trailblazer](https://wiki.postmarketos.org/wiki/PostmarketOS_Trailblazer_(postmarketos-trailblazer))
  now has support for PINE64 PinePhone ({{MR|5703|pmaports}}).
  Thanks {{@Aren}}!

* With latest libadwaita, adaptive windows and dialogs in upstream GTK 4 are
  finally a reality! Therefore it was possible to finally drop our GTK 4 fork
  ({{MR|5622|pmaports}}/[imported](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/5622)).
  Thanks {{@pabloyoyoista}}!

* A new postmarketos-dev package has been added to install bash and coreutils
  by default. We plan to provide -dev variants of some postmarketOS images for
  some devices for use by the Linux Mobile developer community, and in the
  meantime this can be used to conveniently add development tools when building
  your own image
  ({{MR|5180|pmaports}}/[imported](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/5180)).
  Thanks {{@caleb}}, {{@craftyguy}}!

* lk2nd: upgrade to 19.0 and related patches
  ({{MR|5682|pmaports}}/[imported](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/5682),
   {{MR|5691|pmaports}},
   {{MR|5750|pmaports}}).
  Thanks {{@barni2000}}, {{@kouta-kun}}!

* The router
  [linksys-jamaica](https://wiki.postmarketos.org/wiki/Linksys_EA9350_V3_/_MR5500_(linksys-jamaica))
  has gotten (close to) mainline support
  ({{MR|5649|pmaports}}/[imported](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/5649)).
  Thanks {{@exkc}}!

* samsungipcd has been upgrade to v0.3.0, which adds a PPP bypass, so that
  traffic can flow through the native interface (rmnet0) directly, and not via
  the emulated PPP ({{MR|5698|pmaports}}).
  Thanks {{@sleirsgoevy}}!

* Enable flathub by default ({{MR|5726|pmaports}}), if flatpak is installed.
  Thanks {{@craftyguy}}!

* If you ever came across the error "crossdirect: can't handle LD\_PRELOAD":
  why this happens has been researched and the error message has been improved
  to be a lot more helpful ({{MR|5744|pmaports}}).
  Thanks {{@ollieparanoid}}!

* dtbloader: upgrade to 1.2.2, adding 3 new devices
  ({{MR|5737|pmaports}}).
  Thanks {{@TravMurav}}!

* Initial SoC paackges for common configs of msm8226/msm8974 devices have been
  added in ({{MR|5732|pmaports}}).
  Thanks {{@barni2000}}!

* The boot partition of google-x64cros devices was increased to 32 mb
  ({{MR|5730|pmaports}}, [edge post](/edge/2024/11/02/x64cros-32mb-boot-changes/))
  Thanks {{@JustSoup321}}!

* Lots of patches related to systemd
  ({{MR|5631|pmaports}}/[imported](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/5631),
   {{MR|5629|pmaports}}/[imported](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/5629),
   {{MR|5592|pmaports}}/[imported](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/5592),
   {{MR|5597|pmaports}}/[imported](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/5597),
   {{MR|5635|pmaports}}/[imported](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/5635),
   {{MR|5647|pmaports}}/[imported](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/5647),
   {{MR|5651|pmaports}}/[imported](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/5651),
   {{MR|5652|pmaports}}/[imported](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/5652),
   {{MR|5560|pmaports}}/[imported](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/5560),
   {{MR|5661|pmaports}}/[imported](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/5661),
   {{MR|5576|pmaports}}/[imported](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/5576),
   {{MR|5630|pmaports}}/[imported](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/5630),
   {{MR|5685|pmaports}}/[imported](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/5685),
   {{MR|5734|pmaports}}).
  Thanks {{@jane400}}, {{@PureTryOut}}, {{@caleb}}, {{@PureTryOut}},
  {{@JustSoup321}}, {{@craftyguy}}, {{@pabloyoyoista}}, {{@matzipan}}!

* Lots of smaller fixes and improvements
  ({{MR|5625|pmaports}}/[imported](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/5625),
   {{MR|5627|pmaports}}/[imported](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/5627),
   {{MR|5634|pmaports}}/[imported](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/5634),
   {{MR|5610|pmaports}}/[imported](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/5610),
   {{MR|5639|pmaports}}/[imported](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/5639),
   {{MR|5632|pmaports}}/[imported](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/5632),
   {{MR|5653|pmaports}}/[imported](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/5653),
   {{MR|5637|pmaports}}/[imported](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/5637),
   {{MR|5663|pmaports}}/[imported](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/5663),
   {{MR|5678|pmaports}}/[imported](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/5678),
   {{MR|5679|pmaports}}/[imported](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/5679),
   {{MR|5638|pmaports}}/[imported](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/5638),
   {{MR|5642|pmaports}}/[imported](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/5642),
   {{MR|5654|pmaports}}/[imported](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/5654),
   {{MR|5687|pmaports}}/[imported](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/5687),
   {{MR|5643|pmaports}}/[imported](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/5643),
   {{MR|5633|pmaports}}/[imported](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/5633),
   {{MR|5696|pmaports}},
   {{MR|5695|pmaports}},
   {{MR|5699|pmaports}},
   {{MR|5700|pmaports}},
   {{MR|5720|pmaports}},
   {{MR|5721|pmaports}},
   {{MR|5718|pmaports}},
   {{MR|5715|pmaports}},
   {{MR|5722|pmaports}},
   {{MR|5724|pmaports}},
   {{MR|5728|pmaports}},
   {{MR|5714|pmaports}},
   {{MR|5727|pmaports}},
   {{MR|5712|pmaports}},
   {{MR|5735|pmaports}},
   {{MR|5740|pmaports}},
   {{MR|5742|pmaports}},
   {{MR|5743|pmaports}}).
  Thanks {{@Minecrell}}, {{@Jakko}}, {{@craftyguy}}, {{@pabloyoyoista}},
  {{@sleirsgoevy}}, {{@caleb}}, {{@omasanori}}, {{@PureTryOut}}, {{@Adrian}},
  {{@alistair23}}, {{@lucaweiss}}, {{@ollieparanoid}}!


## mobile-config-firefox

* Years ago, by popular demand the navigation bar in mobile-config-firefox was
  moved to the bottom. However, after it was done we also got requests of
  making it configurable so people who prefer it differently can choose. This
  is now possible through an about:config option
  ({{MR|56|mobile-config-firefox}}).
  Thanks {{@1peter10}}!

* Declare touch density
  ({{MR|57|mobile-config-firefox}})
  and add an option (via about:config as above) to give more place to tabs
  ({{MR|58|mobile-config-firefox}}). Both merge requests were opened by
  {{@1peter10}} and are based on an earlier MR from {{@gnumdk}} who started to
  upstream these patches from Droidian into mobile-config-firefox. Thank you
  both!

* Get some more space in the address bar by removing the `->` arrow (you can
  just press return on your on-screen keyboard instead,
  {{MR|55|mobile-config-firefox}}).
  Thanks {{@1peter10}}!

* Fix: let clients detect touch screen
  ({{MR|54|mobile-config-firefox}}/[imported](https://gitlab.com/postmarketOS/mobile-config-firefox/-/merge_requests/54)).
  Thanks {{@SethFalco}}!

## mrhlpr and mrtest

* mrtest/add\_packages.py: add explanation why there might be no artifacts
  ({{MR|49|mrhlpr}}/[imported](https://gitlab.com/postmarketOS/mrhlpr/-/merge_requests/49)).
  Thanks {{@longnoserob}}!

* mrtest: Add support for using an MR as a repo when upgrading
  ({{MR|50|mrhlpr}}/[imported](https://gitlab.com/postmarketOS/mrhlpr/-/merge_requests/50)).
  Thanks {{@Newbyte}}!

* CI: Build native libraries for mrhlpr and mrtest via mypyc (sort of as test
  ground for also doing this with pmbootstrap later on) ({{MR|53|mrhlpr}})
  Thanks {{@Newbyte}}!

* Various small improvements and fixes
  ({{MR|52|mrhlpr}},
   {{MR|46|mrhlpr}}/[imported](https://gitlab.com/postmarketOS/mrhlpr/-/merge_requests/46),
   {{MR|55|mrhlpr}},
   {{MR|56|mrhlpr}},
   {{MR|54|mrhlpr}}).
  Thanks {{@Newbyte}}, {{@longnoserob}}, {{@earboxer}}, {{@pabloyoyoista}}!

## Misc

* xfce4-phone: simplify display power behavior
  ({{MR|11|xfce4-phone}}/[imported](https://gitlab.com/postmarketOS/xfce4-phone/-/merge_requests/11))
  and fix values to disable brightness reduction
  ({{MR|12|xfce4-phone}}/[imported](https://gitlab.com/postmarketOS/xfce4-phone/-/merge_requests/12)).
  Thanks {{@Jakko}}!

* images: trailblazer: build gnome instead of gnome-mobile
  ({{MR|112|build.postmarketos.org}}/[imported](https://gitlab.com/postmarketOS/build.postmarketos.org/-/merge_requests/112)).
  Thanks {{@caleb}}!

* BuffyBox has seen various small improvements
  ({{MR|28|buffybox}}/[imported](https://gitlab.com/postmarketOS/buffybox/-/merge_requests/28),
   {{MR|29|buffybox}}/[imported](https://gitlab.com/postmarketOS/buffybox/-/merge_requests/29),
   {{MR|30|buffybox}}/[imported](https://gitlab.com/postmarketOS/buffybox/-/merge_requests/30),
   {{MR|31|buffybox}}/[imported](https://gitlab.com/postmarketOS/buffybox/-/merge_requests/31),
   {{MR|34|buffybox}}).
  Thanks {{@cherrypicker}}, {{@pabloyoyoista}}, {{@uninsane}}, {{@vladimir.stoyakin}}!

* postmarketos-mkinitfs: allow including initramfs-extra files in the initramfs
  ({{MR|48|postmarketos-mkinitfs}}/[imported](https://gitlab.com/postmarketOS/postmarketos-mkinitfs/-/merge_requests/48)).
  and add compile-time flag to disable Go GC
  ({{MR|56|postmarketos-mkinitfs}}/[imported](https://gitlab.com/postmarketOS/postmarketos-mkinitfs/-/merge_requests/56)).
  Thanks {{@craftyguy}}!

* bpo\_failed\_to\_queued: add arguments for packages/images/bootstraps
  ({{MR|111|build.postmarketos.org}}/[imported](https://gitlab.com/postmarketOS/build.postmarketos.org/-/merge_requests/111)).
  Thanks {{@lucaweiss}}!

* Upstream-compatibility tests have been updated to work with pmbootstrap v3
  ({{MR|14|monitoring}}).
  Thanks {{@lucaweiss}}!

* openrc-settingsd: Implement Slackware specific information for rc.ntpd
  ({{MR|13|openrc-settingsd}}).
  Thanks {{@nater1983}}!

* Changes related to gitlab.com -> gitlab.postmarketos.org move
  ({{MR|113|build.postmarketos.org}},
   {{MR|314|postmarketos.org}},
   {{MR|319|postmarketos.org}},
   {{MR|73|boot-deploy}},
   {{MR|9|ci-common}},
   {{MR|13|monitoring}},
   {{MR|51|mrhlpr}}).
  Thanks {{@lucaweiss}}, {{@ollieparanoid}}, {{@Arnavion}}, {{@celiffe}}!

* Various fixes and improvements in other repositories
  ({{MR|8|ci-common}}/[imported](https://gitlab.com/postmarketOS/ci-common/-/merge_requests/8),
   {{MR|10|ci-common}},
   {{MR|13|ci-common}},
   {{MR|4|swclock-offset}}/[imported](https://gitlab.com/postmarketOS/swclock-offset/-/merge_requests/4),
   {{MR|5|swclock-offset}}/[imported](https://gitlab.com/postmarketOS/swclock-offset/-/merge_requests/5),
   {{MR|3|ttq}}/[imported](https://gitlab.com/postmarketOS/ttq/-/merge_requests/3))
  Thanks {{@Newbyte}}, {{@craftyguy}}, {{@ollieparanoid}}, {{@craftyguy}},
  {{@Jakko}}, {{@longnoserob}}!

## And what's next?

* Since the Mozilla Location Service has been shutdown, we are discussing what
  other service to use instead for geoclue ({{issue|82|postmarketos}}), with
  the goal to enable one soon.

* [SeaGL](https://seagl.org/) is coming up, 2024-11-08 to 09 in Seattle, US.
  {{@anjandev}} will do an
  [Introduction to postmarketOS](https://pretalx.seagl.org/2024/talk/983CCA/)
  talk there, and {{@craftyguy}} will be there as well. Consider stopping by if
  you are in the area!

* The FOSS on mobile devices devroom is back for another round at
  [FOSDEM 2025](https://fosdem.org/2025/)! If you have a great idea for a talk,
  we want to hear from you - have a look at the
  [Call for Proposals](https://gitlab.com/fosdem_mobile/devroom/-/blob/main/README.md)!

## Help wanted

* Edge images for `google-nyan-big` and `google-nyan-blaze` have been disabled,
  as the depthcharge images have become too big and nobody was able to look
  into it. If you have one of these devices and are interested in tweaking
  compression or coming up with another fix, it would be highly appreciated!
  ({{issue|3186|pmaports}})

* Our wiki still has
  [many pages](https://wiki.postmarketos.org/index.php?title=Special:LinkSearch&limit=500&offset=0&target=https%3A%2F%2Fgitlab.com%2FpostmarketOS)
  that link to the legacy repositories at gitlab.com/postmarketOS, including
  people's user pages. If somebody wants to help out with fixing these, please
  do!

* You can send us topics to include in the next blog post by commenting in:
  {{issue|185|postmarketos.org}}

* If you appreciate the work we're doing with postmarketOS and want to support us,
  [consider contributing financially via OpenCollective](https://opencollective.com/postmarketos).
