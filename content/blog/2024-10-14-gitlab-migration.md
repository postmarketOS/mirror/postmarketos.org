title: "Why and how we migrated from gitlab.com to gitlab.postmarketos.org"
date: 2024-10-14
---

We have successfully migrated from gitlab.com to a self-hosted instance at
gitlab.postmarketos.org! This was quite the journey, so here is a blog post
looking back at the reasons, what went well and what didn't, and some lessons
learned that might be useful for other free software projects considering a
similar move. *The reasoning section of this blog post is based on what we
wrote earlier in {{issue|77|postmarketos}}.*

Special thanks to [OSUOSL](https://osuosl.org/) for hosting this GitLab
instance. It has been a pleasure working with them every step of the way!

## Reasoning

### Why move away from gitlab.com?

We are very grateful to the gitlab.com folks for hosting postmarketOS since
2018. They even gave us the ["free premium features for cool open source
projects"](https://handbook.gitlab.com/handbook/marketing/developer-relations/community-programs/open-source-program/)
thing. However the main reason we wanted to move away from gitlab.com was that
the requirements for signing up and using the site have changed dramatically
over the last years.

While in 2018 when we migrated over to gitlab.com, there were no problems accessing the
website and git repositories over for example Tor or setting up an account without giving
any meaningful personal data. Nowadays, **users of gitlab.com are required to
provide a valid phone number and credit card information** when setting up an account.

Also the website is protected by Cloudflare, blocking lots of proxies and
sometimes even making the site hard to use for users with their real IP — event
those who set an account up years ago and have been active since. It is very
understandable that gitlab.com needs to take measures to avoid spam attacks and
prevent misuse of their incredibly useful resources. But that doesn't change the
fact that we've likely lost out on potential contributors due to these policies.
Over and over again we heard from people who would have loved to contribute to
postmarketOS, but have no interest to do so because gitlab.com requires too
much of their personal information.

We highly appreciate that an Open Source Program Manager from GitLab
[reached out](https://gitlab.com/postmarketOS/postmarketos/-/issues/49#note_1730848871)
and asked us for our concerns. So far the situation has not really changed, but
he confirmed that we are not the only community to face these challenges with
GitLab, and he tried to make his team aware of these problems.

### Who is maintaining the instance?

The new gitlab.postmarketos.org instance is maintained by
[OSUOSL](https://osuosl.org/), the Oregon State University Open Source Lab.
They already provided various machines and services to free and open source
projects, including Alpine Linux, Chimera Linux, Gentoo, GNOME, KDE, Kodi,
OpenBSD just to pick a few from this
[huge list](https://osuosl.org/communities/).
[Clayton](/core-contributors/#clayton-craft-craftyguy) talked to them at the
FOSSY conference and they offered to host our GitLab instance too, and provide
us with GitLab runners for x86_64 and aarch64 as well. We have decided that we
will make a one time donation to them (TBD). You will be able to see it on our
[Open Collective](https://opencollective.com/postmarketOS) page just like all
our other spendings.

The benefit for us is that we have mostly the same code forge we have been
using for years (we already tried to avoid using features that are not
available in the self-hosted version of GitLab), but without the aforementioned
GitLab.com-specific problems and without the need to invest time into
maintaining the instance ourselves — time we would much rather spend on moving
postmarketOS forward.

### Why not SourceHut/Forgejo/...?

We think both SourceHut and Forgejo are amazing projects and if you are
considering a move for your project, you should definitively take a closer look
at them! They just weren't the right fit for us at this moment and we wanted
to get it done.

[SourceHut](https://sourcehut.org/): we evaluated it as code forge for some
time with pmbootstrap.git, but then moved it back. See the related
[announcement](/blog/2024/01/17/moving-pmbootstrap/)
for details. postmarketOS has been using SourceHut Builds to build its binary
packages and images since end of 2019 and this works great, we don't have plans
to change that.

[Forgejo](https://forgejo.org/): This is the Gitea fork that Codeberg uses. It
is close to our requirements, but when we checked there were some details that
held us back like not being able to have open discussions block merge requests
/ PRs. It would probably be a good fit if we invested more time and set up our
own CI runners. But at this point we have been looking for an alternative to
gitlab.com for two years already. We are happy that we have a solution with an
easy path forward where we don't need to spend too much energy (that again we
can't spend on improving postmarketOS).

## Preparation

### Announcement

OSUOSL was quick with preparing a GitLab instance for us. We configured DNS to
to point `gitlab.postmarketos.org` to it. Then on 2024-08-25 we created
our announcement {{issue|77|postmarketos}} with:

* An initial target date for the migration (2024-09-15, which changed later).
* Our reasoning (pretty much the text above).
* A TODO list of tasks that we would expand as we go along.

We also announced it in the
[monthly blog post](/blog/2024/08/25/pmOS-update-2024-08/) on the same day.

### Test imports

After making some obvious changes such as uploading our logo, it was time to
do a full test import. How long would it take to migrate our postmarketOS
group? 30 minutes? Two hours? But first we found that there are actually two
ways to import groups and projects.

#### File export vs. direct transfer

[File export](https://docs.gitlab.com/ee/user/project/settings/import_export.html):

* You have to click "export" on gitlab.com and "import" on your instance for
  each and every project. We have over 80 projects, so this would be a lot of
  work.
* Preserving user contributions is said to be working if users with the same
  email address already exist in the target GitLab instance, and if they have
  configured a public email.
* The docs page says "Migrating groups and projects by using direct transfer is
  recommended."

[Direct Transfer](https://docs.gitlab.com/ee/user/group/import/):

* With this we could just import the whole postmarketOS group at once, with all
  its projects.
* This feature is [disabled since July 11, 2024](https://status.gitlab.com/pages/incident/5b36dc6502d06804c08349f7/668f7a2d9592d5175042d74e).
* It seemed like preserving user contributions would work: "If you migrate from
  GitLab.com to self-managed GitLab, an administrator can create users on the
  self-managed GitLab instance."

So this was not great, and the related documentation is hard to read. Only
after reading it several times, it now clear that "preserving user
contributions" might have worked if users had set a
[public email](https://docs.gitlab.com/ee/user/profile/index.html#set-your-public-email)
and a user with the same address had already existed in the self-hosted
instance. Most users of course don't have a public email configured and we
don't want to ask people to do this just for this migration.

#### Trying "file export"

We tested "File export" for our biggest projects pmbootstrap and pmaports.

pmbootstrap:

* ~15 min export time
* ~300 MiB export size
* ~2 hour import time

pmaports:

* ~1 hour export time
* ~2 GiB export size
* (>2 hour import time, hard to say how long exactly because we didn't get an
   email when it was done)

#### Administrator, Administrator and Administrator reacted with 👍

Afterwards we were quite disappointed that "preserving user contributions" did
not work as we expected. Instead of having the original usernames in issues and
merge requests, comments, etc., all of them were replaced with the user that
did the import. We did not set our emails to public, and looking back at it
this might have made it work — but then again asking all of our users to set
their email address to public to have everything imported correctly isn't
practical.

As the direct transfer migration said that "an administrator can create users
on the self-managed GitLab instance", we thought that this would be fixed if
only we could use the direct transfer method. We decided to reach out to GitLab
via the email address that was edited into the top post of the
*[Direct Transfer status, dead for too long](https://gitlab.com/gitlab-org/gitlab/-/issues/469228)*
issue.

We were told to open an issue on
[https://about.gitlab.com/support](https://about.gitlab.com/support/) and
proceeded to do that. It was a bit awkward, because we had to fill an
"emergency support request" as the premium for open source tier doesn't include
any support. However the support people were nice and after some days of back
and forth and waiting for other people at GitLab to make decisions, they asked
for a gitlab.com account name and enabled the direct transfer feature for that
account.

Meanwhile we announced that the migration would be done about a month later
(later settling on the date 2024-10-06). We would either get it working with
"direct transfer" until then, or just use the "file export" method with much
more manual effort.

#### Trying "direct transfer"

Importing all postmarketOS projects took about 18 hours. It mostly worked,
there were some errors regarding the pastebin-like feature that we don't use
anyway and a custom emoji that could not be imported. But nothing important
had failed. However the "preserving user contributions" did not work with this
method here either, even though we did the direct transfer as administrator
and at least one account did already exist in the selfhosted gitlab instance
with the same email address and username! In hindsight, it might again have
worked if we set the emails to public, but this isn't even mentioned in the
"direct transfer" documentation.

We decided to go ahead without "preserving user contributions". At least
the direct transfer allowed us to not manually export and import each and every
project.

To be fair: from privacy perspective it makes a lot of sense that you cannot
just export email addresses from all users that ever contributed / commented
in your project without their consent. And just a few days after our migration
we got an email (apparently along with everybody else who had "direct transfer"
enabled at that time) stating that GitLab is working on an improved solution
for [user contribution and membership mapping](https://docs.gitlab.com/ee/user/project/import/index.html#user-contribution-and-membership-mapping)
that doesn't rely on emails. So hopefully this will be solved for other
projects who want to migrate in the future at some point.

## Migration day

The incredible support folks from OSUOSL helped us ensure that OAuth worked
properly, among other things. And then the migration day came. Given that
the test transfer took 18 hours, we started the migration at midnight. We gave
a heads-up a few hours before on
[status.postmarketos.org](https://status.postmarketos.org/issues/2024-10-06-migration-to-gitlab.postmarketos.org/), and shared that via IRC/matrix and Mastodon.

Then we did the following:

* Started the direct transfer.
* Archived all our repositories on gitlab.com, so they don't change during or
  after the migration.
* Went to sleep since the migration would take a lot of time again.
* Around 18 hours after the import started, it was still ongoing so we posted an
  update to the status page with next steps and estimation that it may take a
  few more hours.

### Timeout during merge request transfer

The import went the same as the test transfer … except the merge requests of
pmaports, just this one repository, went into a timeout for several hundred
merge requests. This was unfortunate, but at that point it would be
unreasonable to start the transfer again and *hope* that everything gets
imported this time. So now some of the merge requests are not fully
transferred. However, the more important part is that all issues of all
projects are imported.

It seems that the merge requests with timeout were not closed correctly, so now
we had several hundred merge requests too many open. In addition to a lot of
merge requests that should still be open, but were not usable for the most part
since they were not owned by the users who originally submitted them any more.
So they could not edit them and would need to submit new merge requests anyway,
unless the authors happened to have push rights for the repository.

We decided to just close all open merge requests with a quick and dirty shell
script:

<details>

```sh
#!/bin/sh
TOKEN="glpat-PUT-TOKEN-HERE"

while true; do
    curl \
        --header "PRIVATE-TOKEN: $TOKEN" \
        "https://yourgitlabinstance/api/v4/merge_requests?scope=all&state=opened" > open2.txt

    PROJECTS="$(cat open2.txt| jq  '.[].project_id' | sort -u)"
    echo "PROJECTS: $PROJECTS"

    if [ -z "$PROJECTS" ]; then
        echo "DONE!"
        exit 0
    fi

    for project in $PROJECTS; do
        OPEN_MRS="$(cat open2.txt| jq ".[] | select(.project_id == $project) | .iid")"
        echo "OPEN_MRS in $project: $OPEN_MRS"

        if [ -z "$OPEN_MRS" ]; then
            echo "DONE!"
            exit 0
        fi

        for i in $OPEN_MRS; do
            echo ":: $i"
            curl \
                --request PUT \
                --header "PRIVATE-TOKEN: $TOKEN" \
                "https://yourgitlabinstance/api/v4/projects/$project/merge_requests/$i?state_event=close"
            echo ""
        done
    done
done
```
</details>

Since then we manually re-opened a few merge requests that still are relevant
and can be pushed to by the original authors (and merged most of them), but at
least we don't have hundreds of irrelevant open MRs cluttering the instance.

## Finishing up

Over the last week we fixed up several things, like CI not working and emails
not arriving from our instance. Besides some smaller things everything seems to
be working again. Let us know if something is not working for you in
{{issue|77|postmarketos}} or the postmarketos-devel
[chat](https://wiki.postmarketos.org/wiki/Matrix_and_IRC).

## Observations / lessons learned

* You can use "direct transfer" if you ask support about it.
* Test the import before migration day.
* "Preserving user contributions" will likely not work until
  [this](https://docs.gitlab.com/ee/user/project/import/index.html#user-contribution-and-membership-mapping)
  is available or unless you set email addresses to public and make sure users
  with the same email exist in your target instance (both untested by us!)
* Use a separate account for the transfer/import such as the Administrator
  account or another one as users which cannot be mapped show up as this one.
  You don't want this to be the account of another person in the project.
* We could have done a better job at making sure most of our CI scripts work
  with the new instance, fixing them up took quite some time afterwards.
* [Admin mode](https://docs.gitlab.com/ee/administration/settings/sign_in_restrictions.html#admin-mode) is great.
* Avoid features that are not available in the free version of self-hosted
  GitLab, such as [epics](https://docs.gitlab.com/ee/user/group/epics/) if you
  plan to migrate at some point. We mostly tried to do this, but had some epics
  and migrating them was a lot of manual effort.

Thanks to
[Oliver](/core-contributors/#oliver-smith-ollieparanoid),
[Clayton](/core-contributors/#clayton-craft-craftyguy),
[Luca](/core-contributors/#luca-weiss-z3ntu),
[Stefan](/core-contributors/#stefan-hansson-newbyte),
[Aster](/trusted-contributors/#aster-boese-justsoup),
for doing migration related tasks, and to
[Lance](https://fosstodon.org/@ramereth)
from OSUOSL for the amazing support, as well as everybody who has signed up
to our new instance and started contributing to postmarketOS through it
already!

If you appreciate the work we're doing with postmarketOS and want to support us,
[consider contributing financially via OpenCollective](https://opencollective.com/postmarketos).
