title: "The Road to systemd"
date: 2024-05-26
---
systemd support for GNOME Mobile, GNOME Shell, Phosh, and KDE Plasma is a very high priority within postmarketOS. See the [initial systemd announcement](https://postmarketos.org/blog/2024/03/05/adding-systemd/) for a refresher on why. To ensure we have enough time for integrating systemd properly into postmarketOS, we're targeting the 24.12 release.

On the Core Team, Caleb and Clayton will be leading the charge. Our new Trusted Contributor, Bryant, will also be helping, focusing on project management and blog posts. Many more people will be involved of course, but they'll be the main drivers.

Alright, let's dive in! 🤿

# pmbootstrap
postmarketOS's dev tooling is a big part of what makes it so great and something we are continually improving. pmbootstrap is the centerpiece, but at 7 years old it's showing its age. Adding systemd support to it will be difficult in its current form, so we're going to spend some time improving pmbootstrap to make that integration easier. Most of the changes will be under-the-hood, but there will be some nice user-facing improvement too! Some have already shipped in the recent 2.3.0 release, but there is more work to do. Read more details about this [in the announcement](https://postmarketos.org/blog/2024/05/19/pmOS-update-2024-05/#pmbootstrap-230-and-feature-freeze).

# Project tracking
Work is being tracked in the [initial systemd support](https://gitlab.com/groups/postmarketOS/-/milestones/22) milestone. We've kept this milestone intentionally very limited, so it only contains what's on the critical path for general systemd support. So it'll be tracking core infrastructure work and not be very device specific. When this milestone is complete everything will be ready to build and install either the systemd-based or the OpenRC-based UIs.

Tracking other issues related to systemd falls to the new and aptly-named [systemd label](https://gitlab.com/groups/postmarketOS/-/issues/?label_name%5B%5D=systemd) that will be used across all postmarketOS projects. Active Community members, Trusted Contributors, and the Core Team have labeling rights, so please add this label if it's missing. Issues with this label will be triaged very actively during this initial development phase.

There's also a new systemd chatroom (see the [Matrix and IRC page](https://wiki.postmarketos.org/wiki/Matrix_and_IRC) on the wiki). Please keep any discussion of systemd in this room. This reduces noise in the other chatrooms and improves communication on the systemd work. We'll likely not keep this room past the 24.12 release as after that systemd issues will be resolved like other issues.

# Current status
It's hard to summarize succinctly the current status as there's a lot of things going on! We're making good progress though and don't foresee any big technical blockers for shipping systemd.

One decision around installing systemd has been made since the announcement: switching to systemd will require a reinstall. We've looked into an update process, but making one that's reliable would be a lot of effort. Since this is a one-time cost for the user, we've decided not to spend our limited developer resources on this.

Your best bet to follow along is to follow this blog or [our social accounts](https://postmarketos.org/chats-and-social-media/). We'll be doing more blogposts like this one, providing status updates just on the systemd work.

# Help wanted
If you would like to have systemd + postmarketOS available as soon as possible, and learn something along the way, then the best way is to get involved yourself. We always need help testing, and you can test systemd today! [Build your own image](https://wiki.postmarketos.org/wiki/Systemd#How_to_build_your_own_systemd-based_postmarketOS_images) with your favorite UI using systemd and try it out. Report any bugs you find and help us fix them! You can also look into other [systemd-labeled issues](https://gitlab.com/groups/postmarketOS/-/issues/?label_name[]=systemd) - especially those with the [`help wanted` label](https://gitlab.com/groups/postmarketOS/-/issues/?label_name[]=systemd&label_name[]=help%20wanted) - if you're looking for more ways to contribute.

If you do want to help with development, remember to join us in the [systemd chatroom](https://wiki.postmarketos.org/wiki/Matrix_and_IRC)!
