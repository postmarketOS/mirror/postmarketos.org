title: "systemd, Part Deux"
title-short: "systemd, Part Deux"
date: 2024-07-24
---
It's been a bit since
[our last systemd post](/blog/2024/05/26/the-road-to-systemd/), but we've been
hard at work, so here's an update!

We've made good progress. The basics are in place and we can boot into both
GNOME Shell and KDE Plasma. However both DEs start services themselves and
don't do so using systemd. We instead want to have all services managed by
systemd: this will make it easier to understand and maintain and also for users
to modify running services. We've figured out how to disable this autostarting
of services on GNOME, but haven't yet found an equivalent with Plasma. If you
know a way, please tell us in
[the tracking issue](https://gitlab.com/postmarketOS/pmaports/-/issues/2972).

A big topic has been figuring out an elegant way to enable services, that is
more robust than the post-install and post-upgrade scripts approach we are
currently using with OpenRC. With the current approach, either users can't
override if they want to have a service enabled or not or we cannot
automatically enable a new service that may be needed on upgrade. The latter
is needed if for example a UI needs a new daemon to run properly, or when
switching from pulseaudio to pipewire, etc.

So we've learned a thing or two about our own requirements for this, and have
been trying to design a way to do the things using systemd-isms. We are working
hard on making the end result more resilient, and it has been quite an
adventure so far that has taken us through the depths of systemd's
documentation (where we couldn't find much about the integration level) and
into tearing apart other distros to see what they do to solve these problems.
Now we are pretty close to a solution!

The systemd functionality currently lives in a separate branch of pmaports.
This being separate from the main codebase means we have to manually keep them
in sync and there's extra manual work to build a systemd image. So we're also
prioritizing getting the systemd work merged into pmaports' `master` branch
(for which the biggest blocker is the
[split systemd repository in bpo](https://gitlab.com/postmarketOS/build.postmarketos.org/-/issues/140)).

With the change to systemd, there will be more packages forked from Alpine
upstream. Maintaining these packages is currently some manual effort:
monitoring when there's a change upstream, modifying the fork, testing, and
merging. The first two steps can be automated, so we're working on that now
([tracking issue](https://gitlab.com/postmarketOS/pmaports/-/issues/2790)).

And the last issue we're prioritizing is better support for the `/usr` merge.
While not strictly necessary for systemd, tackling this now solves some bugs
we've been running into and simplifies maintenance.
[This article](https://lwn.net/Articles/890219/) has a good summary of the
`/usr` merge, but in short, it's about installing binaries in one location
rather than two, which exists for mostly historical reasons not practical ones.
The merge isn't done yet in Alpine, but we have done it in postmarketOS for
systemd based installs by running a fork of apk-tools with a
[custom patch](https://gitlab.alpinelinux.org/alpine/apk-tools/-/merge_requests/155).
After discussing upstream, a better implementation has been suggested and the
patch has been rewritten (now rewriting paths at the point the files are
installed rather than at the database level, the old approach caused some edge
case bugs and broke some advanced apk features). We now need to use and
extensively test the new version of the patch
([tracking issue](https://gitlab.com/postmarketOS/pmaports/-/issues/2988)).

Once the above issues are resolved, we're ready for wider testing! We'll want
help testing the systemd builds and with systemd-ifying more services! This
will be a ton of work, but with our amazing community contributing expect it to
go quickly! We'll put out another public call for testing when we reach this
point. This will probably be in another month or so (you can follow the
progress [here](https://gitlab.com/postmarketOS/pmaports/-/issues/2938)).

Finally, Caleb and Clayton had a talk about using systemd in postmarketOS
accepted to [All Systems Go](https://all-systems-go.io/) in September. This
conference is about critical user-space parts of Linux and will be a great
place to validate our systemd integration work and discuss long-term
maintenance.
