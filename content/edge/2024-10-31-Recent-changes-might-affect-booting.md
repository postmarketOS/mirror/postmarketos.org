title: "Recent changes might affect booting"
date: 2024-10-31
---

Some recent changes to the postmarketOS initramfs and `mkinitfs` tool might cause issues when upgrading or booting devices. When updating over the next few days with `apk upgrade`, please keep an eye out for failures. If `apk` fails, do **not** reboot without first remedying the problem (see below). The changes were tested on a variety of devices, but there's always a risk of breaking boot with changes like this. As always, it's a great idea to back up valuable data, especially when running postmarketOS edge.

If you have problems and need help, please head to the [unified initramfs megathread](https://gitlab.postmarketos.org/postmarketOS/pmaports/-/issues/3279) issue and follow the instructions. Also feel free to [reach out to us on chat](https://wiki.postmarketos.org/wiki/Matrix_and_IRC).

## Also see:

- [Initramfs changes](https://gitlab.postmarketos.org/postmarketOS/pmaports/-/merge_requests/5636)
- [mkinitfs changes](https://gitlab.postmarketos.org/postmarketOS/postmarketos-mkinitfs/-/merge_requests/48)
