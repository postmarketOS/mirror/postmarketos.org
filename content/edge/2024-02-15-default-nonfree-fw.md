title: "Non-free firmware is installed by default"
date: 2024-02-15
---

Traditionally we've tried to give users an option to opt out of installing
proprietary, non-free firmware when building images with pmbootstrap. Recent
changes to pmbootstrap have disabled this mechanism, so now non-free firmware
is always installed if it's needed to support functionality on a given device.
All pre-built postmarketOS images have always included non-free firmware.

The mechanism behind this option was hard to maintain, not well tested, and
sometimes builds would end up having non-free firmware installed anyway. It
forced us to do weird tricks in APKBUILDs, sometimes causing firmware to be
accidentally removed from devices. This has caused some really annoying
regressions for folks, and is likely to be a reliable source of similar trouble
in the future.

In an ideal world, hardware would work without the need to load proprietary
non-free firmware. Unfortunately, we live in a world where this is increasingly
not true. Many bootloaders load a lot of non-free firmware on boot that users
cannot replace or ignore. Many chips, such as the GPU, WiFi, Bluetooth, modem,
video decoding, etc. in your devices require proprietary firmware before
they'll even function at all. Some modern devices can't even be charged without
loading proprietary firmware - the battery will literally drain away while it
is plugged in if this firmware is not loaded!

For the vast majority of cases none of this proprietary, non-free firmware gets
executed on the main processor, it will only be loaded into secondary
processors and run there. Most devices have strong memory separation so a rogue
firmware shouldn't be able to take over the system.

We understand this situation is far from ideal, but after much thought we felt
it was more important to stop spending time on this barely working, not well
tested feature, and instead strive to bring free software to more people and
create a distro that works "out of the box" on as many devices as possible.

- Related: {{MR|2255|pmbootstrap}}
- ["Free Software Policy with Semi-Firm Firmware"](https://archive.org/details/fossy2023_Free_Software_Policy_with_Semi) Talk @ FOSSY 2023
