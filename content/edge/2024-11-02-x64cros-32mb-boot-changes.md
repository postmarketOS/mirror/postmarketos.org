title: "Action required for existing installs on x86_64 Chromebooks"
date: 2024-11-02
---

Recently we increased the boot partition size required to boot postmarketOS on x86_64 Chromebooks, from 16MB to 32MB. This change was made because 16MB is no longer sufficient for storing the kernel + initramfs. Users who have existing installs of pmOS will need to take one of several actions to continue using pmOS on the device. Please note that this only affects x86_64 Chromebooks that are running stock firmware. If you have custom firmware installed, then this change has no impact for you.

### If your Chromebook only supports a 16MB boot partition (aka made around 2015 or earlier)...

In order to run pmOS on this device, you will need to flash custom firmware to it. See the last section below for more information.


### If your Chromebook supports a 32MB or more boot partition...

There are three options to choose between, each with their own benefits/trade-offs::

- Perform a reinstall of pmOS using a new image. Images built after November 2nd should have the fix in them.

- Flash custom firmware, see the last section below for more information. One benefit of doing this is the device would not be affected later in the unlikely event that we have to increase the boot partition size to greater than 32 MB.

- Resize the existing boot partition, making it at least 32 MB. This is tedious and risky, for experienced folks only! Make backups

### About installing custom firmware

 This is not without risks, please make backups of important data. [MrChromebox has a guide for flashing a custom firmware (using Coreboot) to x86_64 Chromebooks here](https://docs.mrchromebox.tech/docs/getting-started.html), However it's currently not possible to flash custom firmware on x86_64 Chromebooks from within pmOS, it'll have to be done through other means until [this issue is resolved](https://gitlab.postmarketos.org/postmarketOS/pmaports/-/issues/3277).

We understand that breaking changes like this can be very inconvenient, and we try hard not to make them. If you have questions or need help, [please visit us on chat](https://wiki.postmarketos.org/wiki/Matrix_and_IRC)!

Also see:

- [Recent change to increase depthcharge boot partition size](https://gitlab.postmarketos.org/postmarketOS/pmaports/-/merge_requests/5730)
2454)
