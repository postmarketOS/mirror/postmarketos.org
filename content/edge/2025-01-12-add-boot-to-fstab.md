title: "postmarketos-initramfs 3.7.0 requires manual action for pmOS installs from 2022 or older"
date: 2025-01-12
---

## Summary

postmarketOS installations from 2022 or older (built with pmbootstrap
[before this patch](https://gitlab.postmarketos.org/postmarketOS/pmbootstrap/-/commit/46789ccee8c75eade289359d2eeba212fe58a800))
that have a separate boot partition but do not have an entry for `/boot` in
`/etc/fstab` may be affected by a change in `postmarketos-initramfs-3.7.0`
that causes `/boot` to no longer be auto-mounted. Such installations will need
to add an entry for `/boot` to `/etc/fstab`. Not all such old installations are
affected, but it is recommended to add the entry anyway, unless your device
specifically does not have a separate boot partition.

## Details

New installations of pmOS mount `/boot` via an entry in `/etc/fstab`.
Before that, installations used to rely on the initramfs script to mount it and
intentionally did not have `/boot` in `/etc/fstab`. Even after new installations
started adding `/boot` to `fstab`, the initramfs script continued to support
such old installations by probing and mounting `/boot` if it wasn't listed in `fstab`.

However this backward-compatibility behavior has been modified in [`postmarketos-initramfs-3.7.0`](https://gitlab.postmarketos.org/postmarketOS/pmaports/-/commit/755132f651f325f967d711c1ee6451c118517dbd)
to only run if the `fstab` is empty. This caters to old installations that did not add
any custom mounts to `fstab`, but installations that did add such custom mounts
will no longer auto-mount `/boot`. This can lead to confusing behavior
of the device, such as broken audio / wifi / cellular modem after a kernel upgrade,
because the device boots an old initramfs with an old kernel from the boot partition
but the old version's modules in rootfs have already been deleted.
Therefore old installations need to explicitly add an entry for `/boot` to `/etc/fstab`.

## Adding a /boot entry manually

The procedure is as follows (all commands must be run as root):

1. Confirm `/boot` isn't already mounted, with `mount | grep /boot`. Unmount it if it is.

2. Run `lsblk -fp`. Look for the line about the partition with label `pmOS_boot`. Note the UUID from this line.

3. Create an entry in `/etc/fstab` that looks like `UUID=$uuid /boot $fstype nodev,nosuid,noexec 0 0`.

4. If a previous kernel upgrade or run of `mkinitfs` already wrote files to `/boot`,
   delete those files so that they do not waste space of the `/` partition.

5. Run `mount /boot` to mount `/boot` per the new `fstab` entry. Make sure it succeeds.

6. Run `apk fix --reinstall your-device-kernel-package` (eg `apk fix --reinstall linux-postmarketos-allwinner` for the Pine64 PinePhone)
   to reinstall the kernel binary into `/boot` and to regenerate the initramfs.
   If the package requires working network to be downloaded but network is broken on your device
   because of missing kernel modules, you may need to download the package file on another device
   shuttle it over to the broken device via USB network, SD card, mass storage mode, etc,
   then install it with `apk add /path/to/kernel.apk`.

7. Reboot.

For example, if the output of step 2 is:

```
NAME                 FSTYPE      FSVER LABEL     UUID                                 FSAVAIL FSUSE% MOUNTPOINTS
/dev/mmcblk2
├─/dev/mmcblk2p1     ext2              pmOS_boot d6e15a04-8138-45d1-ac8c-f170697eb001  172.6M    18%
└─/dev/mmcblk2p2     crypto_LUKS                 3d423349-21b7-4bd5-bdfd-da050605d4d8
  └─/dev/mapper/root ext4              pmOS_root ff9c2b3e-6cad-46eb-bff9-12aee03f5ef6   14.7G    42% /
```

... then the entry in `/etc/fstab` should look like:

```
UUID=d6e15a04-8138-45d1-ac8c-f170697eb001 /boot ext2 nodev,nosuid,noexec 0 0
```

UEFI-boot devices will instead have a `vfat` partition with a smaller UUID, eg:

```
NAME                 FSTYPE      FSVER LABEL     UUID                                 FSAVAIL FSUSE% MOUNTPOINTS
/dev/mmcblk0
├─/dev/mmcblk0p1     vfat              pmOS_boot 5478-197C
```

... for which the entry in `/etc/fstab` should look like:

```
UUID=5478-197C /boot vfat nodev,nosuid,noexec 0 0
```
