title: "Accidental edge repository deletion"
date: 2024-10-25
---

Earlier today, the entirety of the Alpine Linux edge binary package repository was accidentally erased, with the exception of riscv64 packages.

According to the Alpine Linux team, this was due to incorrect invocation of rsync.

As of writing this, the packages have been restored. Run `apk update` as root on your device to synchronize packages; it should work now.

More information:

- [A Mastodon thread from the Alpine Linux team regarding this issue](https://fosstodon.org/@alpinelinux/113369672198203997)
- [The relevant Alpine Linux GitLab issue](https://gitlab.alpinelinux.org/alpine/infra/infra/-/issues/10831)
