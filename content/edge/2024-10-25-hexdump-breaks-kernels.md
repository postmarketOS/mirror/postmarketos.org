title: "busybox hexdump can break some kernels"
date: 2024-10-25
---

A recent update to busybox's hexdump applet has a bug that can result in some kernel packages being built with a broken kernel image file. When this happens, no error is shown by `pmbootstrap --build` or `abuild` during the build step, and no error is shown by `apk` during installation, but the kernel image that is installed is truncated and will not boot.

If you are building kernels for postmarketOS, it's recommended to use the latest pmbootstrap from git to avoid hitting this bug. We have worked around it in pmbootstrap, by requiring the full hexdump app when building stuff. The bug was reported upstream and as of today no fix as been identified.

Also see:

- [Upstream bug report](https://lists.busybox.net/pipermail/busybox/2024-October/090982.html), and [thread debugging it](https://lists.busybox.net/pipermail/busybox/2024-October/090983.html)
- [postmarketOS discussion about the bug](https://gitlab.postmarketos.org/postmarketOS/pmaports/-/issues/3268)
- [Patch adding workaround for postmarketOS](https://gitlab.postmarketos.org/postmarketOS/pmbootstrap/-/merge_requests/2454)
