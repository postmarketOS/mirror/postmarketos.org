title: "Conflict between sudo and doas-sudo-shim (UPDATE: keeping doas-sudo-shim requires manual intervention)"
date: 2024-12-01
---

### UPDATE: doas-sudo-shim needs to be explicitly installed

To fix the problem below (details in {{MR|5867|pmaports}}), the
`doas-sudo-shim` package needs to be explicitly installed from now on. If you
rely on it (you type `sudo` even though `doas` is installed), do this:

```
# doas apk add doas-sudo-shim
```


### "pmbootstrap install" failure

**UPDATE:** this has been resolved with the MR linked above, it will be merged
and packages will be built shortly.

During the install step, a dependency-resolution conflict between sudo and
doas-sudo-shim occurs. The cause of the problem is still under investigation.
It appeared with the latest apk-tools-static in Alpine edge (2.14.6-r0).

```
ERROR: unable to select packages:
  sudo-1.9.16_p1-r1:
    conflicts: doas-sudo-shim-0.1.1-r1[cmd:sudo=1.9.16_p1-r1]
    breaks: doas-sudo-shim-0.1.1-r1[!sudo]
    satisfies: postmarketos-base-38-r0[sudo-virt]
               postmarketos-base-nofde-38-r0[sudo-virt]
  doas-sudo-shim-0.1.1-r1:
    conflicts: sudo-1.9.16_p1-r1[cmd:sudo=0.1.1-r1]
    satisfies: postmarketos-base-doas-38-r0[doas-sudo-shim]
               postmarketos-base-38-r0[sudo-virt]
               postmarketos-base-nofde-38-r0[sudo-virt]
```

Building images for v24.06 still works.

We are looking into this, see links below if you want to read more about it or
help with fixing it.

If you have problems and need help, please
[reach out to us on chat](https://wiki.postmarketos.org/wiki/Matrix_and_IRC).

## Related links:

* {{issue|3340|pmaports}}
* [apk-tools#11045](https://gitlab.alpinelinux.org/alpine/apk-tools/-/issues/11045)
* {{MR|5867|pmaports}}
