title: "dnsmasq upgrade breaks DNS resolution"
date: 2024-02-14
---

The recent upgrade of `dnsmasq` to version 2.90 fixing a security
issue is known to break the system for multiple users. The reason
for this breakage is currently investigated, but yet to be found.

A workaround is applied to `postmarketos-base-ui-networkmanager`
However, your system might prefer a mobile connection over WiFi
until this situation is properly resolved.

## Recovery

If you already upgraded to the new version and need to recover
from there, a solution would be to create a config file that
deactivates the DNS cache:

```
$ cat /etc/NetworkManager/conf.d/51-disable-dnsmasq.conf
[main]
dns=default
```

After a restart, you can see DNS resolution working again. This is
a workaround, and meant to be temporary.

Now, you can run `sudo apk upgrade -a`, to get the new version of
`postmarketos-base-ui-networkmanager` and delete the config file
`/etc/NetworkManager/conf.d/51-disable-dnsmasq.conf` again.

Related: [pmaports#2601](https://gitlab.com/postmarketOS/pmaports/-/issues/2601), [aports#15788](https://gitlab.alpinelinux.org/alpine/aports/-/issues/15788), [pmaports!4824](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/4824)
