title: "Polkit conflict when installing GNOME or Phosh"
date: 2024-12-06
---

Currently trying to install GNOME or Phosh on postmarketOS edge fails with a
conflict error, e.g.
[here](https://builds.sr.ht/~postmarketos/job/1381867#task-img-452).

Workaround: add `polkit,polkit-elogind` to additional packages in
`pmbootstrap init`

## Related links:

* {{issue|3349|pmaports}}
