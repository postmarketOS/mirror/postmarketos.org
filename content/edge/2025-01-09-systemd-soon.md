title: "systemd has been merged into edge"
date: 2025-01-09
---

**UPDATE:** If systemd coming to postmarketOS is news to you, please read
[the original announcement first](/blog/2024/03/05/adding-systemd/).

With the 24.12 stable release out the door and the holidays over, as promised we
will finally be merging the systemd staging branch into pmaports master. We
plan to do this tomorrow (Friday, 2025-01-10). Please note that it will take
some time until all systemd related packages and images are built, and that
pmaports CI and building your own images may not behave as expected on this
day.

The rollout means that images (prebuilts on our downloads page and ones you
build yourself) will use systemd by default on the following UIs initially:

* GNOME (mobile)
* Plasma (mobile)
* Phosh

If you maintain a device in pmaports, especially one we build images for and you
haven't yet had a chance to try out systemd, now is the perfect time to find and
fix any pesky bugs. Notably, device specific services that don't have packaged
systemd unit files will not work!

Check out the [systemd wiki page](https://wiki.postmarketos.org/wiki/Systemd)
for instructions on how to build systemd images yourself and an explanation on
service files.

Please label systemd-specific bugs for your device with the systemd label!

Unit files should be packaged in the
[systemd-services](https://gitlab.postmarketos.org/postmarketOS/pmaports/-/blob/master_staging_systemd/extra-repos/systemd/systemd-services/APKBUILD?ref_type=heads)
package, the APKBUILD has instructions for adding new services.

**UPDATE**: in the chat it was unclear whether existing OpenRC installations
are affected. As the systemd packages live in an extra repository, they should
not affect the OpenRC installations (but as usually when merging big changes
there may be some unexpected fallout). Existing OpenRC installations are not
automatically switched over. You can still select OpenRC in "pmbootstrap init"
after this is merged.

## Update (2025-01-10)

The merge was done, related build.postmarketos.org code changes were merged too
and packages are still building as of writing. **See the current status at
[https://build.postmarketos.org](https://build.postmarketos.org) and
{{issue|140|build.postmarketos.org}}.**

If you were using the staging repository before and want to switch over:

* Wait until the binary packages for your architecture are built!
* Edit `/etc/apk/repositories` and change `staging/systemd` to `extra-repos/systemd`
* `apk update`
* `apk upgrade -a`

## Update (2025-01-12)

Images with systemd are building, and the first ones were built successfully!

* Look for Gnome (Mobile), Plasma (Mobile), Phosh images dated 2025-01-12 or
  newer [here](https://images.postmarketos.org/bpo/edge/).
* If you need service files that we have not packaged yet, see the
  [systemd/services](https://wiki.postmarketos.org/wiki/Systemd/services) wiki
  article.
* Read the [systemd](https://wiki.postmarketos.org/wiki/Systemd) wiki article
  for building your own images with pmbootstrap.

Join the [postmarketos-systemd chat](https://wiki.postmarketos.org/wiki/Matrix_and_IRC)
if you have any questions. Happy hacking!
