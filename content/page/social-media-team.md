title: "Social Media Team"
---
{{@PureTryOut}},
{{@caleb}} and
{{@ollieparanoid}}
are in the Social Media Team and administrate the postmarketOS accounts for the
social media sites mentioned in
[discussion platforms](/explore/#discussion-platforms).

## Guidelines for posting

These guidelines were mostly written with Mastodon in mind, our primary social
media presence. But they apply to others too.

### General

* Use the postmarketOS Mastodon account for boosting other posts, and for
  making announcements about new releases, blog posts, podcasts, etc.
* For replying to posts, use personal accounts instead of the pmOS account.
* When something doesn't fit, let's discuss it first before deleting (except if
  it is completely out of line, then delete it first but let others know
  afterwards).
* The account should get nice feelings across, not make people feel angry.

### What NOT to boost

* Posts that are negative about other projects (i.e. I switched from distro/UI
  X to postmarketOS with UI Y and it's much better).
* Posts that make demands to projects / people that we could collaborate with
  instead, i.e. $upstream should make their browser more adaptive for phones.
  This will just annoy them and cause outrage, not get something done (what
  does get it done is instead collaborating with upstream in their trackers,
  sending patches, etc.).
* Posts warning about people trolling postmarketOS. Talking about them publicly
  on our Mastodon account just encourages them more. Discuss internally about
  e.g. defaced wiki pages and fix them, and move on with more important stuff
  instead of giving them attention.

### What to boost

* People enjoying postmarketOS.
* People working on software / hardware stuff related to postmarketOS.
* Releases of upstream projects we use (new KDE, Sxmo, GNOME, Phosh, Biktor's
  modem FW, Alpine, ... versions) (when we see them &mdash; we don't need to
  specifically look for them and boost all of them, that's too much effort).
* [Linmob's weekly posts](https://linmob.net/): gives a great summary of the
  whole Linux mobile scene, including a lot of the upstream projects we use,
  and oftentimes mentions postmarketOS but is worth posting even if it doesn't
  to emphasize that we are part of this greater linux mobile community!
* Other things that are related in a wider sense, e.g. FSFE upcycling campaigns
  may make sense.
