title: "Frequently Asked Questions"
---

### Why is postmarketOS based on Alpine Linux?

The biggest upside is that Alpine is small. The base installation is about **5 MB**! Thanks to that, our development/installation tool pmbootstrap is able to abstract everything in chroots and therefore keep the development environment consistent, no matter which Linux distribution your host runs on. And if you messed up (or we have a bug), you can simply run <code>pmbootstrap zap</code> and the chroot will be set up again in seconds.

Another benefit of the tininess of Alpine - many older devices don't have much storage space to spare, so small system images can be anything ranging from useful to required.

### Will Android apps be supported?

We support Android apps through [Waydroid](https://wiki.postmarketos.org/wiki/Waydroid)! However, it is highly recommended to use native Linux applications, as there are some downsides to Android apps:

* Freedom issues — most are proprietary, or only work together with a proprietary network. Some even track you. See [F-Droid](https://f-droid.org/) for FOSS programs that respect your freedom.
* Heavy resource usage: You need to run an Android environment inside of postmarketOS. So it uses not only more RAM and storage, but also more CPU and in turn eats more of your battery life compared to native Linux applications.

As such, we recommend at least trying to find a native Linux alternative to your favourite application, or ask yourself if you really need it. For app developers, consider using [Kirigami UI](https://dot.kde.org/2017/01/02/kde-releases-beta-kirigami-ui-20), [MauiKit](https://mauikit.org/), or similar to develop apps for both mainstream mobile OSes (Android/iOS) as well as natively for Linux distributions. Kirigami and MauiKit apps can also be convergent and work well on desktop OSes, too. You can also use [libadwaita](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/) to create great Linux phone and desktop apps, but Android and iOS are unsupported.

### Is dual boot supported?

It is possible to boot postmarketOS from an SD card without overwriting the existing OS if you can load a kernel and initramfs with your USB cable (or load it from the SD card as well, like you can on the Nokia N900). Typically this will work with fastboot devices that have an SD card slot. See [Dual Booting](https://wiki.postmarketos.org/wiki/Dual_Booting) for more details and supported options.

Other dual boot solutions are not supported, but it should not be that hard to add them if you know how these work. See [pmb#421](https://gitlab.com/postmarketOS/pmbootstrap/-/issues/421) for ideas.

On many devices with the A/B partitioning scheme, it is possible to flash postmarketOS to the other/unused slot. The initramfs will find the installation, even if you're currently on the slot with Android.

### Can apps be installed from Flatpak?

Yes, see the [wiki page](https://wiki.postmarketos.org/wiki/Flatpak) for details.

### What about Snaps and AppImages?

Our preferred non-apk app format is Flatpak, so we don't have much interest in supporting other formats.

Snap is not packaged. AppImages [may work](https://github.com/AppImage/AppImageKit/issues/1015).

### Why postmarketOS over ungoogled Android or Halium?

With postmarketOS, we follow an upstream-first philosophy where as often as
feasible we use the upstream versions of software instead of vendor forks. This
means that unlike Android and Halium where you run outdated Linux kernels that
depend on proprietary userspace drivers to work properly, you are instead
running a completely free software userspace and kernel where you never have to
miss out on bugfixes and security fixes because your device's OEM no longer
cares about supporting it. We do still require firmware for features like WiFi
on most devices, which unfortunately in most cases is unavoidable. Still, it is
a major improvement over the proprietary userspace components necessary for
hardware function of Android as this allows us to keep devices supported
indefinitely as long as there are people interested in supporting them.

Another benefit of this approach is that we can share more code with desktop and
server Linux as we use the same programming interfaces as those, unlike Android
which uses many custom interfaces which fundamentally are different from what
desktop Linux uses. This means that we are significantly less tied to Google for
advancement of the operating system. While Android could hypothetically be
forked and developed entirely independently from Google, there is no significant
developer community that could take on such a big endeavour. With desktop Linux,
the community already exists, and we benefit from that. While Google keeps most
of their services out of AOSP, there is still the conflict of interest where
Google benefits from designing Android's user interface and apps in such a way
that it benefits their finances over what benefits the user. An example of this
is how the AOSP core apps have virtually become unmaintained after Google
created services that more or less filled the same purpose as them, except with
some form of monetisation built-in.

### Is there any GUI front-end for package management?

Yes, GNOME Software and Plasma's Discover support apk (postmarketOS and Alpine's package manager, not the Android package format).

### What filesystem is used?

We're using Ext4 right now. F2FS and Btrfs are supported as well, but images using them must be built manually using [pmbootstrap](https://gitlab.com/postmarketOS/pmbootstrap/). There are discussions regarding potentially using Btrfs to allow for rolling back updates: [pma#492](https://gitlab.com/postmarketOS/pmaports/-/issues/492)

### Are all postmarketOS packages and the kernel built locally when creating an installation image?

You build the rootfs image on your PC with binary packages from the postmarketOS binary repository. Missing packages get built locally, but the repository is usually up-to-date. Building packages locally is useful for development where you may want to make changes to them.

### Which USB/Wireless peripherals work?

Many devices have working WiFi, and USB network is available for most devices. See the devices feature matrix for details.

USB mice, keyboards, gamepads et cetera should work just as well as on other Linux distributions. However, you may need to enable support for gamepads and other USB devices in your device's particular kernel before they work — we currently do not enable support for USB peripherals in any particular consistent manner. Additionally, you often need a USB OTG cable to use them on a phone. USB OTG is not supported in mainline on some devices, and not supported by the hardware at all on others.

Some devices also support being hooked up to external monitors.

### Is there a list that explains all the technical abbreviations?

Sure, take a look at the [glossary](https://wiki.postmarketos.org/wiki/Glossary).

### Why don't we use firmware files from Android's firmware partition?

For some devices we do, actually. See [msm-firmware-loader](https://gitlab.com/postmarketOS/msm-firmware-loader).

However, for most devices we instead package the firmware. When we do this, we have the following advantages (see the discussion in [pmb#637](https://gitlab.com/postmarketOS/pmbootstrap/-/issues/637)):

 - We know which version we have
 - We can update it easily (either with official new versions, or with hacks like nexmon)
 - We don't run into conflict with other OSes that may be installed on the device (e.g. when uninstalling postmarketOS and installing another ROM, or when doing NFS boot)
 - This is required for most non-Android devices, which tend to not have such a firmware partition.

### Can project Treble help postmarketOS?

Google introduced [Treble](https://android-developers.googleblog.com/2017/05/here-comes-treble-modular-base-for.html) as a means of improving the update situation for Android devices by having a "vendor implementation" with a stable API that Android builds upon. The idea is that you more easily can switch the Android version because of that API.

You will still have the "vendor implementation", which is different for every device. It contains proprietary userspace drivers for which you as a user depend on the manufacturer to keep updated. This means that after two years or so when the support runs out, you will still have a device that does not get updates any more.

Also keep in mind that this will work for newer Android 8.0 Oreo phones only. We already have an alarmingly high number of phones which will never get that improvement. They can be saved from being electronic waste with projects like postmarketOS.
