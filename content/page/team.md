title: "Who makes postmarketOS?"
---

## Contributors

As a free and open-source software (FOSS) project, anyone can contribute to
postmarketOS by submitting patches and taking part in discussions.
There are a wide range of contributions that come from
all corners of our amazing community!
You too [can contribute](/contribute/)!

All creatures welcome. Make sure to follow the [Code of Conduct](/coc).

### Contributor Groups

Members who have special responsibilities are organized into the following
groups, with a specific set of responsibilities.

#### Core Contributors

The Core Contributors are the most involved with the project. They set the
overall project direction, design and maintain the infrastructure, make
releases, and everything else. The group is currently limited to a maximum of 8
people to keep it effective. We currently don't have a formal process for
becoming a Core Contributor. However if you are already a Trusted Contributor
and have a very good reason, then talk to us and we can consider it.

* [Who are the Core Contributors?](/core-contributors)
* [Meeting minutes](https://wiki.postmarketos.org/wiki/Core_Team_Meetings)

#### Trusted Contributors

Trusted Contributors work on day-to-day things across the project including
development, code reviews, merging changes, triaging issues, community support,
outreach by going to conferences and doing talks, improving documentation
and/or creating artwork. Community members that have been contributing to
postmarketOS in a meaningful way for six months or longer and intend to do so
in the future can apply to become a Trusted Contributor!

* [Who are the Trusted Contributors?](/trusted-contributors)
* [How to become a Trusted Contributor?](/blog/2023/12/03/how-to-become-tc/)
  ([Part 2](/blog/2024/01/28/how-to-become-tc2/))

#### Active Contributors

Active Contributors can't merge changes themselves, but otherwise do much of
what Trusted Contributors do. They usually focus on the specific devices they're
interested in.

* [Who are the Active Contributors?](https://gitlab.postmarketos.org/groups/group/active-contributors/-/group_members)
* [How to become an Active Contributor?](/blog/2024/05/14/active-community-members/)

### Teams

postmarketOS is also organized into Teams, which are built around a singular
topic or purpose.

#### Testing Team

Given the breadth of hardware postmarketOS supports, extensive testing is
important to prevent regressions: the Testing Team tests changes before they're
released ensuring they're as reliable as possible.

* [Who is in the Testing Team / How to join?](https://wiki.postmarketos.org/wiki/Testing_Team)

#### Code of Conduct Team

By following our [Code of Conduct](/coc/), we establish a baseline of civil and
professional collaboration. In case people behave unacceptably (join our
channels and insult others, post spam, etc.) we enforce the CoC with
the Chat Moderators and CoC (mail) team.

* [Who are the chat moderators / how to make a chat moderation request?](/coc/#chat-moderation-request)
* [Who is in the CoC (mail) team / how to contact them?](/coc/#email)

#### The Board

The board is responsible for approving expenses in our
[Open Collective](https://opencollective.com/postmarketos) as well as budgets
when executing projects.

* [Who are the Board Members?](/board)
* [What are the related finance processes?](/financials/)

#### Social Media Team

The Social Media Team administrates the postmarketOS account for the social
media sites mentioned in
[discussion platforms](/explore/#discussion-platforms).

* [Who is in the Social Media Team / what are the guidelines?](/social-media-team)
