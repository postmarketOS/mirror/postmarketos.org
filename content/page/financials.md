title: "Financials"
---

postmarketOS income and expenses are public in our
[Open Collective](https://opencollective.com/postmarketOS). In addition to the
numbers you can see there, this page provides further information.

## Financial blog posts

To provide context to the income and expenses in Open Collective, we regularly
provide updates about our financial situation. You can find a list of those
updates below:

<!-- put new ones on top -->
* 2024-08-11:
  [Short financial update and lookahead](https://postmarketos.org/blog/2024/08/11/pmOS-financial-update/)
  &mdash; Our first summary since we moved to Open Collective

We plan to make future financial summaries roughly every six months.

### Can I submit reimbursements to OpenCollective?

If you are a well known community member and you have bought something that you
need for postmarketOS development, such as phones, SD cards, etc., then you can
submit a reimbursement request through OpenCollective and [the board](board/)
will consider whether to approve it.

Talk to us beforehand if you are not sure whether something would get approved:
<br>board &lt;at&gt; postmarketos.org

Regarding the data that is needed in the expense request,
[OCE docs](https://docs.opencollective.com/oceurope/how-it-works/expenses/invoices-or-reimbursements)
say you need a receipt (attach a PDF or a photo) with the following:

> For a receipt to be approved by our team it must include:
>
> * an amount equal to the amount requested
> * the identification of the vendor (name, address, tax number...)
> * the nature of the expense
> * tax information (VAT rate)
>
> Order confirmation, proforma invoices, card slips and bank statements alone are not valid receipts.

## Expenditure approval process

* [The Board](/board/) reviews each expense.
* All 3 board members must agree with the expense, either via discussion in
  chat, mail, real life or by writing "LGTM" as comment in the expense on
  Open Collective.
* The last board member to approve clicks the Approve button (instead of
  leaving a comment that says that they approve).

Some Core Team members are admins on Open Collective and could in theory
approve expenses, but they must not do it. Approving expenses must only be done
by the Board.

## Executing big projects within postmarketOS

Big projects are assigned by the Core Contributors to be executed by a single
or multiple people. Such big projects often come associated with a budget, be
it through grants or through postmarketOS funds. During their execution:

* Each project will have somebody associated with it doing project management.
* That person will be responsible for usage of funds. There will be monthly
  written or verbal reports to the Core Contributors meetings.
* If the project is financed, be it through grants or through postmarketOS own
  expenses, there should be budget allocated to project management. Project
  management-related expenses in a project are limited to a maximum of 20% of
  the development expenses.
