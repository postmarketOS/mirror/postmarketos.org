title: "Trusted Contributors"
---
<!-- ordered alphabetically -->

## Achill Gilgenast (fossdd)

🏢 *What am I doing?*

* Maintaining GNOME, Phosh and many other related packages in Alpine Linux
* Helping out & fixing issues between Alpine Linux and postmarketOS
* Polishing up the systemd integration
* Fixing various paper-cuts on various upstreams
* Occasionally working on the Linux kernel

🤔 *What am I thinking about?*

* Throwing a immutable postmarketOS on my grandma's laptop
* Pushing postmarketOS into being more reliable & stable

😍 *What do I enjoy?*

* To see your device running postmarketOS
* Watching the Linux Mobile space develop and grow

💬 *Where can you find me?*

* ![GitLab logo](/static/img/icons/gitlab.png){: class="inline"} [@fossdd](https://gitlab.postmarketos.org/fossdd)
* ![Mastodon logo](/static/img/icons/mastodon.svg){: class="inline"} [@fossdd@chaos.social](https://chaos.social/@fossdd)
* ![Matrix logo](/static/img/icons/matrix.svg){: class="inline"} [@fossdd:matrix.org](https://matrix.to/#/@fossdd:matrix.org)

<hr class="team-page-separator">

## Alexey Minnekhanov (minlexx)

🏢 *What am I doing?*

* Maintaining some Qualcomm SoC kernels and device ports
* Working on Linux kernel and bootloaders
* Maintaining plasma-discover apk integration
* Occasionally reviewing merge requests
* Testing releases and MRs on owned devices

🤔 *What am I thinking about?*

* Stability &mdash; how to make things not break with each upgrade
* Making a universal cross-platform postmarketOS installer that supports a wide variety of devices

😍 *What do I enjoy?*

* When things are working as expected out of the box
* Seeing more and more devices booting mainline Linux / U-Boot

💬 *Where can you find me?*


* ![GitLab logo](/static/img/icons/gitlab.png){: class="inline"} [@minlexx](https://gitlab.postmarketos.org/alexeymin)
* ![Mastodon logo](/static/img/icons/mastodon.svg){: class="inline"} [@alexeymin@fosstodon.org](https://fosstodon.org/@alexeymin)
* ![Matrix logo](/static/img/icons/matrix.svg){: class="inline"} [@alexey.min:kde.org](https://matrix.to/#/@alexey.min:kde.org)

<hr class="team-page-separator">

## Anjan Momi (anjandev)

🏢 *What am I doing?*

* Maintaining Sxmo
* Maintaining Alpine Linux packages
* Writing userspace applications for postmarketOS
* Occasionally reviewing merge requests

🤔 *What am I thinking about?*

* Improving userspace
* Creating a hacker-friendly phone OS!
* Promoting postmarketOS

😍 *What do I enjoy?*

* Tools that are easy to use and follow the Unix philosophy
* Free software!

💬 *Where can you find me?*


* ![GitLab logo](/static/img/icons/gitlab.png){: class="inline"} [@anjandev](https://gitlab.postmarketos.org/anjandev)
* ![Mastodon logo](/static/img/icons/mastodon.svg){: class="inline"} [@anjan@pleroma.debian.social](https://pleroma.debian.social/users/anjan)
* ![Matrix logo](/static/img/icons/matrix.svg){: class="inline"} [@anjan:postmarketos.org](https://matrix.to/#/@anjan:postmarketos.org)

<hr class="team-page-separator">

## Anton Bambura (jenneron)

🏢 *What am I doing?*

* Implementing the depthcharge bootloader support used on Chrome OS devices and other contributions related to Chrome OS devices
* Maintaining and mainlining AYN Odin
* Improving the boot process
* Helping in the chat with Chromebooks
* Responding to pmaports issues
* Reviewing merge requests

🤔 *What am I thinking about?*

* Making postmarketOS userspace more suitable for various form factors
* Shipping full firmware and secondary bootloaders via [fwupd](https://github.com/fwupd/fwupd)

😍 *What do I enjoy?*

* Automating things
* Improving the user experience
* Combining devices into generic ports

💬 *Where can you find me?*

* ![GitLab logo](/static/img/icons/gitlab.png){: class="inline"} [@jenneron](https://gitlab.postmarketos.org/jenneron)
* ![Matrix logo](/static/img/icons/matrix.svg){: class="inline"} [@jenneron:matrix.org](https://matrix.to/#/@jenneron:matrix.org) (find me in `main` and `devel` postmarketOS rooms)

<hr class="team-page-separator">

## Arnav Singh (arnavion)

🏢 *What am I doing?*

Maintaining support for the PinePhone.

🤔 *What am I thinking about?*

Making pmOS better.

😍 *What do I enjoy?*

Free and open-source software, also known as FOSS!

💬 *Where can you find me?*

* ![GitLab logo](/static/img/icons/gitlab.png){: class="inline"} [@Arnavion](https://gitlab.postmarketos.org/Arnavion)
* `Arnavion` on IRC

<hr class="team-page-separator">

## Aster Boese (justsoup)

🏢 *What am I doing?*

* Solidifying systemd support
* Implementing immutability
* Maintaining Lomiri packages
* Occasionally tinkering with the oneplus-fajita

🤔 *What am I thinking about?*

* How to make pmOS easier to understand
* How can Lomiri work better on Alpine

😍 *What do I enjoy?*

* Packaging for Alpine
* Programming in Rust

💬 *Where can you find me?*

* ![GitLab logo](/static/img/icons/gitlab.png){: class="inline"} [@JustSoup321](https://gitlab.postmarketos.org/JustSoup321)
* ![Mastodon logo](/static/img/icons/mastodon.svg){: class="inline"} [@justsoup@mstdn.social](https://mstdn.social/@justsoup)
* ![Matrix logo](/static/img/icons/matrix.svg){: class="inline"} [@justsomesoup:matrix.org](https://matrix.to/#/@justsomesoup:matrix.org)

<hr class="team-page-separator">

## Barnabás Czémán (barni2000)

🏢 *What am I doing?*

* Maintaining some Qualcomm SoC kernels and device ports
* Working on Linux kernel
* Mainlining some Qualcomm SoC
* Occasionally reviewing merge requests
* Testing releases and MRs on owned devices

🤔 *What am I thinking about?*

* How to make maintaining easier
* How to reduce downstream ports

😍 *What do I enjoy?*

* Porting postmarketOS to more device
* Seeing more and more devices booting mainline Linux
* Enabling more features time to time

💬 *Where can you find me?*

* ![GitLab logo](/static/img/icons/gitlab.png){: class="inline"} [@barni2000](https://gitlab.postmarketos.org/barni2000)
* ![Matrix logo](/static/img/icons/matrix.svg){: class="inline"} [@barni2000:matrix.org](https://matrix.to/#/@barni2000:matrix.org)

<hr class="team-page-separator">

## Dylan Van Assche (dylanvanassche)

🏢 *What am I doing?*

* Reverse engineering things like VoLTE, Bluetooth HFP, Qualcomm sensors, etc.
* Helping with improving community and main devices like Fairphone 4/5 and SDM845 devices
* Working on middleware: PipeWire, fwupd, ModemManager, iio-sensor-proxy, etc.

🤔*What am I thinking about?*

* How to provide privacy-respecting services essential for the complete phone user experience by self-hosting or working with other trusted providers (geolocation, A-GPS data, connectivity check, DNS, etc.)

<hr class="team-page-separator">

## Federico Amedeo Izzo (fizzo)

🏢 *What am I doing?*

* Daily driving oneplus-enchilada and fixing the paper cuts I encounter
* Porting postmarketOS to every Linux-based device I lay my hands on
* Learning Rust so I can contribute to apps like [Railway](https://mobile.schmidhuberj.de/railway) or [phosh-ev](https://gitlab.gnome.org/guidog/phosh-ev/)

🤔 *What am I thinking about?*

* Freeing my digital life from proprietary systems and services
* Getting postmarketOS ready for more technical users

😍 *What do I enjoy?*

* Sane defaults
* Reducing power consumption of devices
* Color calibration of screens

💬 *Where can you find me?*

* ![GitLab logo](/static/img/icons/gitlab.png){: class="inline"} [@f-izzo](https://gitlab.postmarketos.org/f-izzo)
* ![Mastodon logo](/static/img/icons/mastodon.svg){: class="inline"} [@fizzo@chaos.social](https://chaos.social/@fizzo)
* ![Matrix logo](/static/img/icons/matrix.svg){: class="inline"} [@fizzo:matrix.org](https://matrix.to/#/@fizzo:matrix.org)
* 🌐 [https://izzo.pro/](https://izzo.pro/)

<hr class="team-page-separator">

## Ferass El Hafidi (funderscore)

🏢 *What am I doing?*

* Reviewing merge requests
* Maintaining ports to Amlogic devices
* Helping folks in the chat

🤔 *What am I thinking about?*

* Bringing postmarketOS to set-top boxes
* Liberating devices

😍 *What do I enjoy?*

* Helping new users successfully boot postmarketOS &mdash; it's quite rewarding!
* Running postmarketOS on my devices with FOSS boot firmware
* Writing code
* Interacting with friendly communities centered around postmarketOS!

💬 *Where can you find me?*

* ![GitLab logo](/static/img/icons/gitlab.png){: class="inline"} [@funderscore](https://gitlab.postmarketos.org/funderscore)
* `f_` and `f_[x]` on [OFTC IRC](https://www.oftc.net/)
* 🌐 [https://vitali64.duckdns.org/](https://vitali64.duckdns.org/)

<hr class="team-page-separator">

## Henrik Grimler (Grimler)

🏢 *What am I doing?*

* Reviewing merge requests
* Maintaining ports to for Samsung devices
* Helping (or confusing? 😅) people in the different chats
* Trying to bring Linux to more Samsung Exynos devices (mostly Exynos 5420 with some work on Exynos 8895)
* Maintain ports for Exynos 4 and Exynos 5 devices
* Exploring the PCBs of different Exynos devices to find debug points and other useful connectors

🤔 *What am I thinking about?*

* The different models and drivers for the Maxim family of fuel gauges and how to best support them for specific devices

😍 *What do I enjoy?*

* Solving issues and making things work &mdash; there's nothing more satisfying than successfully booting Linux or a bootloader on a device!
* Helping others improve their skill sets by sharing what I know
* Learning about hardware and low-level things

💬 *Where can you find me?*

* ![GitLab logo](/static/img/icons/gitlab.png){: class="inline"} [@grimler](https://gitlab.postmarketos.org/grimler)
* `grimler` on IRC
* ![Mastodon logo](/static/img/icons/mastodon.svg){: class="inline"} [@grimler@mastodon.social](https://mastodon.social/@grimler)
* ![Matrix logo](/static/img/icons/matrix.svg){: class="inline"} [@grimler:matrix.org](https://matrix.to/#/@grimler:matrix.org)
* 🌐 [https://grimler.se](https://grimler.se)

<hr class="team-page-separator">

## jane400

🏢 *What am I doing?*

* Maintaining systemd on pmOS
* Triaging issues and reviewing merge requests from time to time
* Investigating issues and providing fixes/work-arounds/hacks for them :3

🤔 *What am I thinking about?*

* How can we stabilize our software stack?
* And how can we better cross-distro development and standardization?

😍 *What do I enjoy?*

* Reverse engineering a german android app from a certain parcel delivery company (more details soon :P)
* Dabbling a tiny bit with gtk4/libadwaita
* Toying around with Qualcomm devices on mainline linux

💬 *Where can you find me?*

* GitLab: [`@jane400`](https://gitlab.postmarketos.org/jane400)
* Matrix: `@jane400:greyseal.eu`
* Mastodon: [`@jane@smolhaj.social`](https://smolhaj.social/@jane)

<hr class="team-page-separator">

## Minecrell

🚧 *This trusted contributor does amazing work. No description here yet though.*

<hr class="team-page-separator">

## Nikita Travkin (travmurav)

🏢 *What am I doing?*

* Keeping an eye on the MSM8916 platform
* Working on the Linux kernel
* Maintaining [lk2nd](https://github.com/msm8916-mainline/lk2nd)
* Playing with ARM laptops

🤔 *What am I thinking about?*

* Boards booting in a generic manner
* Letting the OS target a common boot path, moving the bootloader and DTB outside of OS

😍 *What do I enjoy?*

* Seeing things being upstreamed
* Vendor lock-in being broken
* Pushing devices beyond expectations

💬 *Where can you find me?*

* 📧 [nikita@trvn.ru](mailto:nikita@trvn.ru)
* ![GitLab logo](/static/img/icons/gitlab.png){: class="inline"} [@TravMurav](https://gitlab.postmarketos.org/TravMurav)
* ![Matrix logo](/static/img/icons/matrix.svg){: class="inline"} [@travmurav:matrix.org](https://matrix.to/#/@travmurav:matrix.org)

<hr class="team-page-separator">

## Raymond Hackley <br> (wonderful&ZeroWidthSpace;ShrineMaiden&ZeroWidthSpace;OfParadise)

🏢 *What am I doing?*

* Maintaining ported MSM8916/MSM8939/MSM8909/MSM8953/Exynos4 devices
* Sending patches to the Linux kernel upstream
* Supporting users in chat and issues with mainline kernel device trees issues
* Coding on postmarketOS related projects
* Reviewing merge requests

🤔 *What am I thinking about?*

* Discovering new uses of phones, such as for home servers
* Reducing and reusing electronic waste
* Bringing postmarketOS, Alpine, and the Linux kernel to old devices

😍 *What do I enjoy?*

* Using Nextcloud/Grocy/FreshRSS/Gitea hosted on my phones
* Watching users successfully booting phones with postmarketOS + mainline kernel

💬 *Where can you find me?*

* ![GitLab logo](/static/img/icons/gitlab.png){: class="inline"} [@wonderfulShrineMaidenOfParadise](https://gitlab.postmarketos.org/wonderfulShrineMaidenOfParadise)
* ![Matrix logo](/static/img/icons/matrix.svg){: class="inline"} @wonderfulShrineMaidenOfParadise in #msm8916-mainline:postmarketos.org

<hr class="team-page-separator">

## Robert Eckelmann (longnoserob)

🏢 *What am I doing?*

* Supporting users in chat and GitLab issues
* Reviewing merge requests
* Testing changes (mostly on nvidia-tegra-armv7 and qcom-sdm845 devices)

🤔 *What am I thinking about?*

* Improving the getting started phase for new users
* Improving the feedback from the users to the developers (mostly by triaging issues)

😍 *What do I enjoy?*

* Happy users after successfully troubleshooting an issue
* Getting new features working and shared with others
* Using older hardware with the latest software

💬 *Where can you find me?*

* ![GitLab logo](/static/img/icons/gitlab.png){: class="inline"} [@longnoserob](https://gitlab.postmarketos.org/longnoserob)
* ![Mastodon logo](/static/img/icons/mastodon.svg){: class="inline"} [@longnoserob@famichiki.jp](https://famichiki.jp/@longnoserob)
* ![Matrix logo](/static/img/icons/matrix.svg){: class="inline"} [@longnoserob:matrix.org](https://matrix.to/#/@longnoserob:matrix.org)
