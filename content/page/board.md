title: "The Board"
---
The Board has 3 natural persons that are elected for either 12 or 24 months. At
the end of the term, the Core Contributors vote who becomes the new
board members.

## 2024-08

* Luca Weiss (chair) (elected for 24 months)
* Oliver Smith
* Stefan Hansson

## 2023-08

Board member Bart Ribbers was appointed on 2021-08 for a term of 24 months,
meaning that the board seat was up for renewal. Board member Martijn Braam
stepped down from the board before the end of his term.

* Luca Weiss (chair)
* Oliver Smith (elected for 24 months)
* Stefan Hansson (elected for 24 months)

In 2023-12 postmarketOS
[moved its finances to Open Collective](/blog/2023/12/14/pmos-on-oc/).
We agreed on a new
[expenditure approval process](/financials/#expenditure-approval-process).
Before Open Collective, we had our finances under the
[Commons Conservacy](https://commonsconservancy.org/) umbrella.

## 2022-08

* Luca Weiss (chair) (elected for 24 months)
* Martijn Braam (elected for 24 months)
* Bart Ribbers

## 2021-08

* Luca Weiss (chair)
* Martijn Braam
* Bart Ribbers (elected for 24 months)

## 2020-08

* Luca Weiss (chair) (elected for 24 months)
* Martijn Braam (elected for 24 months)
* Bart Ribbers (elected for 12 months)
