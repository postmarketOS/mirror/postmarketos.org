title: "Explore postmarketOS"
---

Do you want to help make postmarketOS better? Check how to
[contribute](/contribute)!

### 🧠 Knowledge

* [**Blog**](/blog) &mdash;
  Release announcements and other news
* [**FAQ**](/faq) &mdash;
  Frequently Asked Questions for people new to the project
* [**Podcast**](https://cast.postmarketos.org/) &mdash;
  Long-form discussions and interviews with the wider Linux Mobile scene
* [**Wiki**](https://wiki.postmarketos.org/) &mdash;
  Lots of articles about
  [porting](https://wiki.postmarketos.org/wiki/Porting_to_a_new_device),
  [devices](https://wiki.postmarketos.org/wiki/Devices),
  [UIs](https://wiki.postmarketos.org/wiki/Category:Interface),
  [mainlining](https://wiki.postmarketos.org/wiki/Mainlining)
  and more

### 🚧 Development

* [**Edge Blog**](/edge) &mdash;
  Breakage notes for the development version of postmarketOS
* [**Source Code**](/source-code/) &mdash;
  We proudly license all of our work under free software licenses
* [**Package Search**](https://pkgs.postmarketos.org/packages) &mdash;
  Find package versions, dependencies and contents
* [**BPO**](https://build.postmarketos.org) &mdash;
  Look at the queue and logs of package and image builds
* [**Status**](https://status.postmarketos.org) &mdash;
  Check this page if something seems to be down

### 💬 Discussion Platforms

* [**Matrix/IRC**](https://wiki.postmarketos.org/wiki/Matrix_and_IRC) &mdash;
  Get help with using or developing postmarketOS in real time
* [**Mastodon**](https://fosstodon.org/@postmarketOS) &mdash;
  Mostly boosting cool postmarketOS ports and developments from around the world
* [**Bluesky**](https://bsky.app/profile/postmarketos.org) &mdash;
  We prefer Mastodon as it is built on values closer to ours, but you can read
  our posts on Bluesky too
* [**Lemmy**](https://lemmy.ml/c/postmarketos) &mdash;
  Threaded discussions on links and questions
* [**Events**](/events) &mdash;
  IRL conferences with postmarketOS developers, talks and demos

### 🗃️ Organizational

* [**PMCR**](https://gitlab.postmarketos.org/postmarketOS/pmcr/) &mdash;
  Our change request process for discussing important changes and new features
  with focus and structure
* [**Financials**](/financials) &mdash;
  How we transparently spend money from donations to keep the lights on and to
  move postmarketOS forward
* [**Code of Conduct**](/coc) &mdash;
  How we make our community a great experience for everyone and what to do if
  people are behaving unacceptably

### 👕 Clothing

* [**Merch**](/merch) &mdash;
  Shirts with postmarketOS logos, a cat chewing on an N900 and what not
