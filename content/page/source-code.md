title: "Source Code"
---

### git repositories

Find postmarketOS related git repositories at
[https://gitlab.postmarketos.org](https://gitlab.postmarketos.org). Here are
the most important ones:

  * [pmaports](https://gitlab.postmarketos.org/postmarketOS/pmaports) holds all package
    build recipes.
    <br><small>It is similar to Alpine's
    [aports](https://gitlab.alpinelinux.org/alpine/aports/). The various
    APKBUILDs point to upstream source code for all packages.</small>
  * [pmbootstrap](https://gitlab.postmarketos.org/postmarketOS/pmbootstrap) is the
    sophisticated chroot/build/flash tool to develop and install postmarketOS.
    <br><small>Targeted at power users and developers, for the easy way to
    install postmarketOS, see [/install](/install/).</small>

#### Linux kernel

The [SoC Communities](https://wiki.postmarketos.org/wiki/SoC_Communities) page
lists where SoC specific kernel patches are developed. Find which kernel (with
source) is exactly used by which device by looking at the dependencies of the
`device-` packages in pmaports.

### Issues

Report postmarketOS specific bugs in the pmaports repository, unless they
clearly belong into another project (e.g. pmbootstrap). Upstream bugs (Alpine,
Phosh, Plasma Mobile, ...) should be reported in upstream projects. Read
[how to report issues](https://wiki.postmarketos.org/wiki/How_to_report_issues)
for details.

### Binary packages

If you are looking for the source code, you might also be interested in how the
packages from pmaports get transformed into the official binary packages. This
happens transparently on
[build.postmarketos.org](https://build.postmarketos.org) (bpo). Whenever a git
commit gets pushed/merged to pmaports, bpo calculates which packages need to be
rebuilt, and then builds the missing packages with pmbootstrap on
[builds.sr.ht](https://builds.sr.ht/). All build logs are publicly visible (and
can even be followed while the packages are building).

Built packages can be accessed from the
[official mirrors](https://mirrors.postmarketos.org/), and one can search
through them on
[pkgs.postmarketos.org](https://pkgs.postmarketos.org) (just like
[pkgs.alpinelinux.org](https://pkgs.alpinelinux.org)).
