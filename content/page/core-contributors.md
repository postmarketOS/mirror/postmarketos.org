title: "Core Contributors"
---
<!-- ordered alphabetically -->

![](/static/img/avatars/puretryout.png){: class="avatar"}
## Bart Ribbers (PureTryOut) {: class="contributor-name"}

🏢 *What am I doing?*

* General package maintenance
* Maintaining KDE Plasma and everything related to it in pmOS/Alpine, including general desktop stuff like PipeWire

🤔 *What am I thinking about?*

* Building a private and trustworthy operating system for everyone to use no matter their preferences

😍 *What do I enjoy?*

* Seeing how the Linux desktop modernizes and improves and applying such improvements to my own installations and systems
* Going to open-source related conferences like FOSDEM and KDE Akademy to meet like-minded people

💬 *Where can you find me?*

* ![GitLab logo](/static/img/icons/gitlab.png){: class="inline"} [@PureTryOut](https://gitlab.postmarketos.org/PureTryOut)
* ![Matrix logo](/static/img/icons/matrix.svg){: class="inline"} [@PureTryOut:matrix.org](https://matrix.to/#/@PureTryOut:matrix.org)
* 🌐 [https://fam-ribbers.com](https://fam-ribbers.com)

<hr class="team-page-separator">

![](/static/img/avatars/calebccff.png){: class="avatar"}
## Caleb Connolly (calebccff) {: class="contributor-name"}

🏢 *What am I doing?*

* Maintaining [Snapdragon 845 devices](https://wiki.postmarketos.org/wiki/Qualcomm_Snapdragon_845/850_(SDM845/SDM850))
* U-Boot development for Qualcomm platforms
* Device bringup and kernel development
* Improving tooling and debugging features
* Building CI 😵‍💫
* Occasionally funnyposting on the [@postmarketOS](https://fosstodon/@postmarketOS) fediverse account

🤔 *What am I thinking about?*

* Developer experience and tooling
* Community building
* Doing immutability without making development and tinkering harder
* Whatever this week's annoying driver bug is

😍 *What do I enjoy?*

* Hacking on hardware bringup
* Getting new features working and having people actually be able to use them
* Being part of such a fun community of folks building all kinds of things

💬 *Where can you find me?*

* ![GitLab logo](/static/img/icons/gitlab.png){: class="inline"} [@calebccff](https://gitlab.postmarketos.org/caleb)
* ![Mastodon logo](/static/img/icons/mastodon.svg){: class="inline"} [@cas@social.treehouse.systems](https://social.treehouse.systems/@cas)
* ![Matrix logo](/static/img/icons/matrix.svg){: class="inline"} [@caleb:postmarketos.org](https://matrix.to/#/@caleb:postmarketos.org)

<hr class="team-page-separator">

![](/static/img/avatars/craftyguy.png){: class="avatar"}
## Clayton Craft (craftyguy) {: class="contributor-name"}

🏢 *What am I doing?*

* Working full time on postmarketOS
* Fostering the amazing community
* Helping to maintain a few things around here
* Trying to [be a zero](https://archive.org/details/isbn_9780316253017/page/n9/mode/2up)

🤔 *What am I thinking about?*

* Can we make something I can give to my favorite humans, without feeling gross about it?
* Let's put postmarketOS on everything! Laptops, phones, desktops, tablets, smart watches, TVs, …
* Does anyone on chat have an interesting problem?

😍 *What do I enjoy?*

* Helping others
* Fixing stuff
* Finishing stuff
* Learning new things
* Free software, of course

💬 *Where can you find me?*

* ![GitLab logo](/static/img/icons/gitlab.png){: class="inline"} [@craftyguy](https://gitlab.postmarketos.org/craftyguy)
* ![Matrix logo](/static/img/icons/matrix.svg){: class="inline"} [@craftyguy:postmarketos.org](https://matrix.to/#/@craftyguy:postmarketos.org)
* ![Mastodon logo](/static/img/icons/mastodon.svg){: class="inline"} [@craftyguy@freeradical.zone](https://freeradical.zone/@craftyguy)

<hr class="team-page-separator">

![](/static/img/avatars/z3ntu.png){: class="avatar"}
## Luca Weiss (z3ntu) {: class="contributor-name"}

🏢 *What am I doing?*

* Maintaining infrastructure, like the postmarketOS server and everything that entails
* Mainline kernel development and maintenance for many SoCs (msm8226, msm8974, sm6350, sc7280, and sometimes msm8953)
* Adding eSIM support via [lpac](https://github.com/estkme-group/lpac/issues/41) and building a mobile-friendly UI to manage them.
* Maintaining 100+ packages in Alpine Linux

🤔 *What am I thinking about?*

* Expanding my knowledge sharing with longer-form blog posts and videos like the [mainlining.dev blog](https://mainlining.dev/)
* Improving the user experience around installing postmarketOS from clarifying installation instructions to improving the installer and [webflasher](https://flash.postmarketos.org/)

😍 *What do I enjoy?*

* Improving device functionality in upstream: upstreaming existing code and adding new functionality

💬 *Where can you find me?*

* ![GitLab logo](/static/img/icons/gitlab.png){: class="inline"} [@z3ntu](https://gitlab.postmarketos.org/lucaweiss)
* ![Mastodon logo](/static/img/icons/mastodon.svg){: class="inline"} [@z3ntu@fosstodon.org](https://fosstodon.org/@z3ntu)
* ![Matrix logo](/static/img/icons/matrix.svg){: class="inline"} [@luca:z3ntu.xyz](https://matrix.to/#/@luca:z3ntu.xyz)
* 🌐 [https://lucaweiss.eu/](https://lucaweiss.eu/)

<hr class="team-page-separator">

![](/static/img/avatars/ollieparanoid.png){: class="avatar"}
## Oliver Smith (ollieparanoid) {: class="contributor-name"}

🏢 *What am I doing?*

* Started postmarketOS in 2016, [published it](/blog/2017/05/26/intro/) in 2017
* Hacking, mostly on [pmbootstrap](https://wiki.postmarketos.org/wiki/Pmbootstrap), [build.postmarketos.org](https://build.postmarketos.org), and [pmaports](https://wiki.postmarketos.org/wiki/Pmaports)
* Making postmarketOS releases
* Reviewing code and helping with issues as I find time :)
* Co-maintaining the infrastructure
* Creative things:
    * (Co-)writing [blog posts](/blog)
    * Running the [postmarketOS podcast](https://cast.postmarketos.org/)
    * Making [shirts](/merch) and [stickers](https://gitlab.postmarketos.org/postmarketOS/artwork/-/tree/master/stickers)
* Various organizational tasks

🤔 *What am I thinking about?*

* Getting postmarketOS ready for non-experts (while still keeping it fun for hackers!)
* Building a friendly and respectful community
* Contributing towards a healthy, wider Linux (Mobile) ecosystem
* How to not be a part of the attention economy and helping towards fixing the climate crisis

😍 *What do I enjoy?*

* Running postmarketOS with mainline Linux on my phone and laptop
* Getting into a flow state while coding or doing a creative task
* Meeting people through conferences, Jitsi calls, or the podcast and talking about free software
* Seeing what amazing things people are doing with postmarketOS (it is incredibly inspiring!)

💬 *Where can you find me?*

* ![GitLab logo](/static/img/icons/gitlab.png){: class="inline"} [@ollieparanoid](https://gitlab.postmarketos.org/ollieparanoid)
* ![Mastodon logo](/static/img/icons/mastodon.svg){: class="inline"} [@ollieparanoid@fosstodon.org](https://fosstodon.org/@ollieparanoid)
* ![Matrix logo](/static/img/icons/matrix.svg){: class="inline"} [@ollieparanoid:postmarketos.org](https://matrix.to/#/@ollieparanoid:postmarketos.org)

<hr class="team-page-separator">

![](/static/img/avatars/pabloyoyoista.png){: class="avatar"}
## Pablo Correa Gómez (pabloyoyoista) {: class="contributor-name"}

🏢 *What am I doing?*

* Coordinating and planning around the project priorities
* Searching and applying for grants, including helping others with it
* Meeting different people in the ecosystem, networking and looking for
  opportunities for postmarketOS
* Maintaining GNOME in alpine and in postmarketOS, including the social aspect
  of collaborating with their community, and improving on integration

🤔 *What am I thinking about?*

* How to organically grow and finance postmarketOS as a pioneer of Linux on
  Mobile, including governance, legal, economical, and community affairs
* How to build sustaining, effective, and long-term relationships with related
  FOSS projects

😍 *What do I enjoy?*

* Talking with people about social issues around FOSS and interactions with
  communities
* The usually-abandoned business side of FOSS
* Debugging issues and fixing compatibility problems between upstreams and us

💬 *Where can you find me?*

* 📧 [pabloyoyoista@postmarketos.org](mailto:pabloyoyoista@postmarketos.org)
* ![GitLab logo](/static/img/icons/gitlab.png){: class="inline"} [@pabloyoyoista](https://gitlab.postmarketos.org/pabloyoyoista)
* ![Mastodon logo](/static/img/icons/mastodon.svg){: class="inline"} [@pabloyoyoista@social.treehouse.systems](https://social.treehouse.systems/@pabloyoyoista)
* ![Matrix logo](/static/img/icons/matrix.svg){: class="inline"} [@pabloyoyoista:matrix.org](https://matrix.to/#/@pabloyoyoista:matrix.org)

<hr class="team-page-separator">

![](/static/img/avatars/newbyte.png){: class="avatar"}
## Stefan Hansson (Newbyte) {: class="contributor-name"}

🏢 *What am I doing?*

* Maintaining Phosh and GNOME in Alpine
* Triaging and resolving reported issues &mdash; as much as I can at least! 😅
* Other improvements as I get inspired

🤔 *What am I thinking about?*

* How to turn postmarketOS into a reliable production system

😍 *What do I enjoy?*

* Writing code!
* Fixing usability problems that bug me in `pmbootstrap`
* Low-level kernel development &mdash; Just starting out with this!
* Fixing bugs I encounter in other software projects

💬 *Where can you find me?*

* ![GitLab logo](/static/img/icons/gitlab.png){: class="inline"} [@Newbyte](https://gitlab.postmarketos.org/Newbyte)
* ![Mastodon logo](/static/img/icons/mastodon.svg){: class="inline"} [@newbyte@mastodon.nu](https://mastodon.nu/@newbyte)
* ![Matrix logo](/static/img/icons/matrix.svg){: class="inline"} [@newbyte:matrix.org](https://matrix.to/#/@newbyte:matrix.org)
