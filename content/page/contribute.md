title: "Where to Contribute ❤️"
---

Hello there, glad you are reading this page! We have a broad spectrum of topics
to contribute, and some of them do not even require programming or scripting
knowledge. With each contribution, you don't just get to help out your fellow
postmarketOS friends, but there's always a chance to learn something new and
sharpening your skills along the way. And of course fun, fame and epic stories
to tell to your grandchildren about what a badass contributor you have been.
Now pick what interests you most.

## Reporting issues

Reporting issues you found can be of high value for developers and other users.
With that being said, processing issue reports can also take a lot of time. The
amount of time can be minimized by the following:

* [Search the issue tracker](https://gitlab.postmarketos.org/postmarketOS/pmaports/-/issues)
  to find if your issue was already reported.
* Try to figure out which exact circumstances trigger the problem.
* When reporting a new issue, please include all potentially relevant information.

[➡️ Report a new issue](https://gitlab.postmarketos.org/postmarketOS/pmaports/-/issues/new)

[#grid side#]
![cute little pmOS holding wiki book](/static/img/pmOS-activities-contribution/logo-wiki.svg){: class="w300" }
[#grid text#]
## Documentation

Most of our documentation is [in the wiki](https://wiki.postmarketos.org). We
have pages on devices, tooling, UIs and more. You can help improve these pages
from simple spelling and grammar fixes to making more complex changes.

[➡️ Read the wiki editor's guide](https://wiki.postmarketos.org/wiki/Help:Wiki_editor%27s_guide)
[#grid end#]

[#grid side#]
![cute little pmOS holding soldering iron and multimeter probes](/static/img/pmOS-activities-contribution/logo-hacking.svg){: class="w300" }
[#grid text#]
## Testing

If you have already familiarized yourself with postmarketOS a bit, consider
joining the Testing Team to get cool new stuff first and help with making the
project more stable for your particular device:

* New changes to postmarketOS edge
* New postmarketOS releases

You can do this no matter which device you run postmarketOS on &mdash; QEMU,
phones, tablets, chromebooks or other laptops, SBCs, smartwatches, etc.

[➡️  Join the Testing Team](https://wiki.postmarketos.org/wiki/Testing_Team)
[#grid end#]


[#grid side#]
<p style="text-align: right">
<img alt="cute pmOS buddy holding git" src="/static/img/pmOS-activities-contribution/logo-gitlab.svg" class="w300">
</p>
[#grid text#]

## Development

Suitable tasks to get a start in postmarketOS development:

* [Port](https://wiki.postmarketos.org/wiki/Porting_guide) and
  [mainline](https://wiki.postmarketos.org/wiki/Mainlining_Guide) devices you
  own.
* Fix a small bug that you encounter while using postmarketOS.

[➡️ Take me to the source code](/source-code/)
[#grid end#]

## Contributing upstream

postmarketOS builds upon a lot of other free software projects. Consider
getting involved upstream:

###### [![Alpine Linux](../../static/img/pmOS-contributing-upstream-carousel/alpine.svg)](https://wiki.alpinelinux.org/wiki/Alpine_Linux:Contribute) [![Phosh](../../static/img/pmOS-contributing-upstream-carousel/phosh.svg)](https://phosh.mobi/faq/#whats-a-good-way-to-contribute) [![Plasma Mobile](../../static/img/pmOS-contributing-upstream-carousel/plasma-mobile.svg)](https://plasma-mobile.org/join/) [![GNOME](../../static/img/pmOS-contributing-upstream-carousel/gnome-mobile.svg)](https://welcome.gnome.org/) [![SXMO](../../static/img/pmOS-contributing-upstream-carousel/sxmo.svg)](https://sxmo.org/contribute)

<p style="text-align: center;">
<a href="https://wiki.alpinelinux.org/wiki/Alpine_Linux:Contribute">Alpine Linux</a> •
<a href="https://phosh.mobi/faq/#whats-a-good-way-to-contribute">Phosh</a> •
<a href="https://plasma-mobile.org/join/">Plasma Mobile</a> •
<a href="https://welcome.gnome.org/">GNOME</a> •
<a href="https://sxmo.org/contribute">SXMO</a>
</p>

## Helping other people
[#grid side#]
![two cute little pmOSs](../../static/img/pmOS-activities-contribution/logo-friends.svg){: class="w300" }
[#grid text#]
Get active in the chatrooms (Matrix, IRC), the
[issue tracker](https://gitlab.postmarketos.org/postmarketOS/pmaports/-/issues)
or other discussion platforms and help others along the way,
with motivation or practical tips on whatever they are doing.

[➡️ Discussion platforms](/explore/#discussion-platforms)
[#grid end#]

## Contributing financially
If you like postmarketOS and would like to help us with keeping our
infrastructure running, accelerating the development and more then you can also
make a donation. We list our finances transparently on our OpenCollective page.

[➡️ Go to our OpenCollective page](https://opencollective.com/postmarketOS)

## See also
* [Contributing for (advanced?) Dummies](https://linmob.net/contributing-for-advanced-dummies/)
* [How you can help the Linux Mobile ecosystem](https://connolly.tech/posts/2022_09_16-how-to-help-linux-mobile/)
