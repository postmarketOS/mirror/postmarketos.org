#!/usr/bin/env python3
# Copyright 2024 Oliver Smith
# SPDX-License-Identifier: AGPL-3.0-or-later
# Tool for quickly adding usernames to config/usernames.py
import argparse
import html
import json
import os
import sys
import urllib
import urllib.request

sys.path.insert(1, os.path.realpath(os.path.join(__file__, "../../config")))
import usernames as CONFIG_USERNAMES

ARGS = None


def parse_args():
    global ARGS

    parser = argparse.ArgumentParser()
    sub = parser.add_subparsers(title="action", dest="action", required=True)

    add = sub.add_parser("add", help="add a user to the config")
    add.add_argument("username",
                        help="from gitlab.postmarketos.org")
    add.add_argument("--displayname", help="use this displayname instead of fetching from gitlab")

    ARGS = parser.parse_args()


def check_existing():
    if ARGS.username in CONFIG_USERNAMES.names_and_links:
        print(f"ERROR: username is already in config/usernames.py: {ARGS.username}")
        sys.exit(1)


def get_displayname():
    api_url = f"https://gitlab.postmarketos.org/api/v4/users?username={html.escape(ARGS.username)}"
    print(f"GET {api_url}")
    with urllib.request.urlopen(api_url) as response:
        data = json.load(response)

    if not data:
        print("ERROR: user not found!")
        sys.exit(1)

    return data[0]["name"].split(" ", 1)[0]


def get_updated_names_and_links(displayname):
    username = ARGS.username
    url = f"https://gitlab.postmarketos.org/{html.escape(username)}"

    names_and_links = CONFIG_USERNAMES.names_and_links
    names_and_links[username] = [displayname, url]
    names_sorted = sorted(names_and_links, key=str.casefold)

    ret = {}
    for name in names_sorted:
        ret[name] = names_and_links[name]
    return ret


def update_config(names_and_links):
    varstart = "\nnames_and_links = {\n"
    config_path = os.path.realpath(os.path.join(__file__, "../../config/usernames.py"))
    with open(config_path, "r") as file:
        config_old = file.read()

    header, rest = config_old.split(varstart, 1)
    _, footer = rest.split("}", 1)

    config_new = f"{header}{varstart}"
    for name in names_and_links:
        config_new += f'    "{name}": ["{names_and_links[name][0]}", "{names_and_links[name][1]}"],\n'

    config_new += "}"
    config_new += footer

    with open(config_path, "w") as file:
        file.write(config_new)


def main():
    parse_args()
    check_existing()
    displayname = ARGS.displayname or get_displayname()
    names_and_links = get_updated_names_and_links(displayname)
    update_config(names_and_links)

main()
