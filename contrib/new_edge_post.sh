#!/bin/sh

read -p 'Enter post title: ' title

short_title="$(echo "$title" | tr -s ' ' '-')"
today="$(date +'%Y-%m-%d')"
filename="content/edge/$today-$short_title.md"
cat << EOF > "$filename"
title: "$title"
date: $today
---

TODO: Briefly describe what the problem is, what causes it, and who (UI, use
case, device, ??) is affected by it.

TODO: Describe any workarounds, fixes, or actions user needs to take

TODO: Include any other useful info, but please keep it short. Edge posts often
communicate things that cause folks some grief/annoyance so keep this in mind.
It's important to respect reader time :)

If you have problems and need help, please
[reach out to us on chat](https://wiki.postmarketos.org/wiki/Matrix_and_IRC)
or
[file a new issue in our bug tracker](https://gitlab.postmarketos.org/postmarketOS/pmaports/-/issues).

## Related links:

TODO: list relevant links, like to MRs or issues in pmOS gitlab or upstream.
-
-
EOF

[ -n "$EDITOR" ] && "$EDITOR" "$filename"
