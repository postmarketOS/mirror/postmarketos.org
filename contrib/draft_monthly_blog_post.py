#!/usr/bin/env python3
# Copyright 2024 Oliver Smith
# SPDX-License-Identifier: AGPL-3.0-or-later
# Create a draft for a monthly blog post with gitlab merge requests formatted
# as markdown
import argparse
import datetime
import gitlab as gitlab_api
import html
import json
import os
import subprocess
import sys
import urllib
import urllib.request


sys.path.insert(1, os.path.realpath(os.path.join(__file__, "../../config")))
import usernames as config_usernames
USERNAMES = list(config_usernames.names_and_links.keys())

ARGS = None


def get_blog_post_path():
    date_split = f"{ARGS.title}".split("-")
    year = date_split[0]
    month = date_split[1]
    path = f"content/blog/{ARGS.date}-pmOS-update-{year}-{month}.md"
    return path, year, month


def parse_args():
    global ARGS

    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--date", help="(YYYY-MM-DD) future blog post date", required=True)
    parser.add_argument("-s", "--since", help="(YYYY-MM-DD) look at MRs since this day", required=True)
    parser.add_argument("-t", "--title", help="(YYYY-MM) title: pmOS in YYYY-MM", required=True)
    parser.add_argument("-f", "--force", help="overwrite existing blog post", action="store_true")

    ARGS = parser.parse_args()

    # Verify date format
    datetime.datetime.strptime(ARGS.date, "%Y-%m-%d")
    datetime.datetime.strptime(ARGS.since, "%Y-%m-%d")
    datetime.datetime.strptime(ARGS.title, "%Y-%m")

    path, _, _ = get_blog_post_path()
    if not ARGS.force and os.path.exists(path):
        print(f"ERROR: blog post exists: {path}")
        sys.exit(1)


def gitlab_auth():
    print("Authenticating with gitlab.postmarketos.org")
    gitlab_token = os.environ.get("GITLAB_TOKEN")
    if not gitlab_token:
        raise RuntimeError("Need a read-only API GITLAB_TOKEN!")
    gl = gitlab_api.Gitlab(url="https://gitlab.postmarketos.org", private_token=gitlab_token)
    gl.auth()
    return gl


def add_user(username, displayname):
    global USERNAMES

    print(f"    adding @{username} ({displayname}) to config/usernames.py")
    USERNAMES += [username]

    cmd = ["contrib/username_config_tool.py", "add", username, "--displayname", displayname]
    subprocess.run(cmd, check=True)


def check_title(entry, keywords):
    title = entry["title"].lower()

    for keyword in keywords:
        if keyword.lower() in title:
            return True

    return False


def get_merge_requests(gl):
    print("Iterating over merge requests")
    since = datetime.datetime.strptime(ARGS.since, "%Y-%m-%d")

    ret = {}
    for mr in gl.mergerequests.list(iterator=True, scope="all", state="merged",
                                    order_by="merged_at", sort="desc"):
        ref = mr.attributes["references"]["full"]  # postmarketOS/pmaports!1234
        group = ref.split("/")[0]  # postmarketOS
        project = ref.split("/")[1].split("!")[0]  # pmaports
        username = mr.attributes["author"]["username"]

        entry = {
            "id": mr.attributes["iid"],
            "title": mr.attributes["title"],
            "username": username,
            "group": group,
            "project": project,
        }

        print(f"  [{project}!{entry['id']}] {entry['title']} by @{username}")

        # merged_at: 2024-10-22T11:56:30.171Z
        merge_day = mr.attributes["merged_at"].split("T")[0]
        merge_day = datetime.datetime.strptime(merge_day, "%Y-%m-%d")
        if merge_day < since:
            print(f"  ... was merged before {since} -> not for draft, stop iterating")
            break

        if group != "postmarketOS":
            print(f"  -> WARNING: group is not postmarketOS, skipping: {group}")
            continue

        # Username is "root" for imported merge requests
        if username != "root" and username not in USERNAMES:
            add_user(username, mr.attributes["author"]["name"].split(" ")[0])

        section = project

        keywords = [
                "kconfigcheck",
                "kernel",
                "linux-",
                "mainline",
        ]
        if project == "pmaports" and check_title(entry, keywords):
            section = "Kernel packaging"

        if project == "pmaports" and check_title(entry, ["new device"]):
            section = "New device ports"

        if project == "pmaports" and check_title(entry, ["systemd"]):
            section = "systemd"

        if project == "pmaports" and check_title(entry, ["initramfs"]):
            section = "initramfs"

        if project in ["postmarketos.org", "artwork"]:
            section = "Artwork and homepage"

        if section not in ret:
            ret[section] = []
        ret[section] += [entry]

        print(f"  -> section: {section}")

    # Reverse the order so oldest MRs are on top
    for section in ret.keys():
        ret[section].reverse()

    return ret


def get_sections(sections_found):
    """Get a useful default order for the sections in the blog post. We can
       still move them around or adjust this list later on as it makes sense,
       but this is much better than having a random order of sections."""

    # Roughly sorted by what has an obvious impact on users
    order = [
        "pmbootstrap",
        "User Interfaces",
        "New device ports",
        "Kernel packaging",
        "Device specific changes",
        "systemd",
        "initramfs",
        "pmaports",
        "buffybox",
        "mobile-config-firefox",
        "postmarketos-mkinitfs",
        "...",
        "build.postmarketos.org",
        "mrhlpr",
        "Artwork and homepage",
    ]

    ret = []
    sections_found = sorted(sections_found)

    for section in order:
        if section == "...":
            for section_rest in sections_found:
                if section_rest not in order:
                    ret += [section_rest]
        elif section in sections_found:
            ret += [section]

    return ret


def write_blog_post_draft(mrs):
    path, year, month = get_blog_post_path()
    print(f"Writing blog post draft to: {path}")

    monthly_issues_url = "https://gitlab.postmarketos.org/postmarketOS/postmarketos.org/-/issues/?sort=created_asc&state=opened&label_name%5B%5D=monthly-blog-post&first_page_size=20"
    meetings_url = "https://wiki.postmarketos.org/wiki/Core_Contributors_Meetings"

    with open(path, "w") as f:
        f.write(f'title: "postmarketOS in {year}-{month}: FIXME"\n')
        f.write(f'date: {ARGS.date}\n')
        f.write(f'preview: "{year}-{month}/monthly.jpg"\n')
        f.write('---\n')
        f.write('\n')
        f.write(f'[![FIXME-description](/static/img/{year}-{month}/monthly.jpg)')
        f.write('{: class="wfull" }]')
        f.write(f'(/static/img/{year}-{month}/monthly.jpg)\n')
        f.write('\n')
        f.write('FIXME: intro text\n')
        f.write('\n')
        f.write('## Organizational\n')
        f.write('\n')
        f.write(f'* FIXME (see [monthly blog post issue]({monthly_issues_url})\n')
        f.write(f'  and [core contributor meetings]({meetings_url}))\n')
        f.write('\n')

        for section in get_sections(mrs.keys()):
            f.write(f'## {section}\n')
            f.write('\n')
            for mr in mrs[section]:
                title = mr["title"].replace("_", "\\_")
                f.write(f'* {title}\n')
                f.write(f'  ({{{{MR|{mr["id"]}|{mr["project"]}}}}}')

                if mr["username"] == "root":
                    url = f"https://gitlab.com/postmarketOS/{mr['project']}/-/merge_requests/{mr['id']}"
                    f.write(f'/[imported]({url})).\n')
                    f.write('  Thanks FIXME!\n')
                else:
                    f.write(').\n')
                    f.write(f'  Thanks {{{{@{mr["username"]}}}}}!\n')
                f.write('\n')

        f.write("## Misc\n")
        f.write('\n')
        f.write('* FIXME (other cool stuff that has been done in the community, new podcasts, ...)\n')
        f.write('\n')
        f.write("## And what's next?\n")
        f.write('\n')
        f.write('* FIXME\n')
        f.write('\n')
        f.write("## Help wanted\n")
        f.write('\n')
        f.write("* FIXME: other topics that we want to draw attention to this month?\n")
        f.write('\n')
        f.write("<!-- TODO: create new issue for next blog post and link it below, see:\n")
        f.write(f"     {monthly_issues_url}\n")
        f.write("-->\n")
        f.write("* You can send us topics to include in the next blog post by commenting in: FIXME.\n")
        f.write('\n')
        f.write("* If you appreciate the work we're doing with postmarketOS and want to support us,\n")
        f.write("  [consider contributing financially via OpenCollective](https://opencollective.com/postmarketos).\n")


def main():
    parse_args()
    gl = gitlab_auth()
    mrs = get_merge_requests(gl)
    write_blog_post_draft(mrs)
    print("DONE!")


main()
