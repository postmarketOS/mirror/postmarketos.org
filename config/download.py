# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: AGPL-3.0-or-later

imgs_url = "https://images.postmarketos.org/bpo"

# Both are without the service pack! Usually we don't generate new images after
# publishing a service pack, people are expected to install the service pack
# through update functionality. This means, we might have just announced a
# service pack, but the images don't include it yet.
latest_release = "v24.12"


# Add '"not_in_stable": True' if we only have pre-built images for edge.
table = {
    "Community": {
        "arrow-db410c": {
            "name": "Arrow DragonBoard 410c",
             "vendor": "Arrow",
        },
        "asus-me176c": {
            "name": "ASUS MeMO Pad 7",
            "vendor": "ASUS",
        },
        "bq-paella": {
            "name": "BQ Aquaris X5",
            "vendor": "BQ",
        },
        "fairphone-fp4": {
            "name": "Fairphone 4",
            "vendor": "Fairphone",
        },
        "qcom-msm8953": {
            "name": "Generic MSM8953",
            "vendor": "Generic",
        },
        "generic-x86_64": {
            "name": "Generic x86_64 EFI System",
             "vendor": "Generic",
        },
        "google-gru": {
            "name": "Google Gru Chromebooks",
            "vendor": "Google",
        },
        "google-kukui": {
            "name": "Google Kukui Chromebooks",
            "vendor": "Google",
        },
        "google-oak": {
            "name": "Google Oak Chromebooks",
            "vendor": "Google",
        },
        "google-peach-pit": {
            "name": "Samsung Chromebook 2 11.6\"",
            "vendor": "Samsung",
        },
        "google-sargo": {
            "name": "Google Pixel 3a",
            "vendor": "Google",
        },
        "google-snow": {
            "name": "Samsung Chromebook",
            "vendor": "Samsung",
        },
        "google-trogdor": {
            "name": "Google Trogdor Chromebooks",
            "vendor": "Google",
        },
        "google-veyron": {
            "name": "Google Veyron Chromebooks",
            "vendor": "Google",
        },
        "google-x64cros": {
            "name": "Google Chromebooks with x64 CPU",
            "vendor": "Google",
        },
        "lenovo-a6000": {
            "name": "Lenovo A6000",
            "vendor": "Lenovo",
        },
        "lenovo-a6010": {
            "name": "Lenovo A6010",
            "vendor": "Lenovo",
        },
        "lenovo-21bx": {
            "name": "Lenovo X13s",
            "vendor": "Lenovo",
        },
        "microsoft-surface-rt": {
            "name": "Microsoft Surface RT",
            "vendor": "Microsoft",
        },
        "motorola-harpia": {
            "name": "Motorola Moto G4 Play",
            "vendor": "Motorola",
        },
        "nokia-n900": {
            "name": "Nokia N900",
            "vendor": "Nokia",
        },
        "nvidia-tegra-armv7": {
            "name": "Nvidia Tegra armv7",
            "vendor": "Nvidia",
        },
        "odroid-xu4": {
            "name": "ODROID XU4",
            "vendor": "ODROID",
        },
        "oneplus-enchilada": {
            "name": "OnePlus 6",
            "vendor": "OnePlus",
        },
        "oneplus-fajita": {
            "name": "OnePlus 6T",
            "vendor": "OnePlus",
        },
        "pine64-pinebookpro": {
            "name": "PINE64 PineBook Pro",
            "vendor": "PINE64",
        },
        "pine64-pinephone": {
            "name": "PINE64 PinePhone",
            "vendor": "PINE64",
        },
        "pine64-pinephonepro": {
            "name": "PINE64 PinePhone Pro",
            "vendor": "PINE64",
        },
        "pine64-rockpro64": {
            "name": "PINE64 RockPro 64",
            "vendor": "PINE64",
        },
        "purism-librem5": {
            "name": "Purism Librem 5",
             "vendor": "Purism",
        },
        "samsung-a3": {
            "name": "Samsung Galaxy A3 (2015)",
            "vendor": "Samsung",
        },
        "samsung-a5": {
            "name": "Samsung Galaxy A5 (2015)",
            "vendor": "Samsung",
        },
        "samsung-e7": {
            "name": "Samsung Galaxy E7",
            "vendor": "Samsung",
        },
        "samsung-espresso10": {
            "name": "Samsung Galaxy Tab 2 10.1\"",
            "vendor": "Samsung",
        },
        "samsung-espresso7": {
            "name": "Samsung Galaxy Tab 2 7.0",
            "vendor": "Samsung",
        },
        "samsung-serranove": {
            "name": "Samsung Galaxy S4 Mini Value Edition",
            "vendor": "Samsung",
        },
        "samsung-grandmax": {
            "name": "Samsung Galaxy Grand Max",
            "vendor": "Samsung",
        },
        "samsung-gt510": {
            "name": "Samsung Galaxy Tab A 9.7 (2015)",
            "vendor": "Samsung",
        },
        "samsung-gt58": {
            "name": "Samsung Galaxy Tab A 8.0 (2015)",
            "vendor": "Samsung",
        },
        "samsung-m0": {
            "name": "Samsung Galaxy S III (GT-I9300/SHW-M440S)",
            "vendor": "Samsung",
        },
        "samsung-manta": {
            "name": "Google Nexus 10",
            "vendor": "Samsung",
        },
        "shift-axolotl": {
            "name": "SHIFT6mq",
            "vendor": "SHIFT",
        },
        "wileyfox-crackling": {
            "name": "Wileyfox Swift",
            "vendor": "Wileyfox",
        },
        "xiaomi-beryllium": {
            "name": "Xiaomi Pocophone F1",
            "vendor": "Xiaomi",
        },
        "xiaomi-davinci": {
            "name": "Xiaomi Mi 9T / Redmi K20",
            "vendor": "Xiaomi",
            "not_in_stable": True,
        },
        "xiaomi-scorpio": {
            "name": "Xiaomi Mi Note 2",
            "vendor": "Xiaomi",
        },
        "xiaomi-surya": {
            "name": "Xiaomi POCO X3 NFC",
            "vendor": "Xiaomi",
            "not_in_stable": True,
        },
        "xiaomi-wt88047": {
            "name": "Xiaomi Redmi 2",
            "vendor": "Xiaomi",
        },
    },
    "Testing": {
        "fairphone-fp5": {
            "name": "Fairphone 5",
             "vendor": "Fairphone",
        },
        "google-asurada": {
            "name": "Google Asurada Chromebooks",
             "vendor": "Google",
        },
        "google-cherry": {
            "name": "Google Cherry Chromebooks",
             "vendor": "Google",
        },
        "google-corsola": {
            "name": "Google Corsola Chromebooks",
             "vendor": "Google",
        },
        "google-nyan-big": {
            "name": "Acer Chromebook 13 CB5-311",
            "vendor": "Acer",
        },
        "google-nyan-blaze": {
            "name": "HP Chromebook 14 G3",
             "vendor": "HP",
        },
        "postmarketos-trailblazer": {
            "name": "postmarketOS Trailblazer",
             "vendor": "PostmarketOS",
            "not_in_stable": True,
        },
        "samsung-coreprimevelte": {
            "name": "Samsung Galaxy Core Prime Value Edition LTE",
            "vendor": "Samsung",
        },
    },
}
