# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: AGPL-3.0-or-later
import flask
import glob
import os

# same dir
import config
import blog

template_args = None


def init_global_template_args():
    """ Initialize the global template_args variable once. """
    global template_args

    template_args = {}


def render_template(*args, **kwargs):
    """ Wrapper for flask.render_template, where we can pre-define passed
        arguments, so they will be available for every template (e.g. latest
        news blog post.) """
    global template_args

    if not template_args:
        init_global_template_args()

    return flask.render_template(*args, **kwargs, **template_args)
