#!/bin/sh -e
# Description: editorconfig-checker: lint for trailing whitespaces etc.
# https://postmarketos.org/pmb-ci

if [ "$(id -u)" = 0 ]; then
	set -x
	apk -q add \
		editorconfig-checker
	exec su "${TESTUSER:-build}" -c "sh -e $0"
fi

set -x

# disable-indent-size: false positives, doesn't work with indent style of
# 		       multi-line values with indenting to the key length like
# 		       here
ec \
	-disable-indent-size \
	--exclude static/slides
